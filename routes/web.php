<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['guest']], function () {
    Route::get('/', 'Website\HomeController@index')->name('index');
    Route::get('/events', 'Website\EventController@index')->name('event');
    Route::post('/events/search', 'Website\EventController@searchindex')->name('event.search');
    Route::get('/events/filter', 'Website\EventController@filter')->name('event.filter');
    Route::get('/about-us', 'Website\ManagerController@index')->name('about-us');
    Route::get('/gallery', 'Website\GalleryController@index')->name('gallery');
    Route::get('/gallery/filter', 'Website\GalleryController@filter')->name('gallery.filter');
    Route::post('/feedback', 'Website\HomeController@mailFeedback')->name('feedback');
    Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login/cek','Auth\LoginController@login')->name('login.check'); 
});

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'System\HomeController@index')->name('dashboard.index');
        Route::get('/chart/penerimaan', 'System\HomeController@chartPenerimaan');
        Route::get('/chart/pengeluaran', 'System\HomeController@chartPengeluaran');
        Route::get('/chart/zakat', 'System\HomeController@chartZakat');
        Route::get('/chart/infaq', 'System\HomeController@chartInfaq');      
        Route::get('/logout', 'System\HomeController@logout')->name('logout');

        Route::get('/pengguna/{id}', 'System\PenggunaController@edit')->name('pengguna.edit');
        Route::post('/pengguna/ubah', 'System\PenggunaController@update')->name('pengguna.update');

        Route::get('/galeri', 'System\GaleriController@index')->name('galeri.index');
        Route::post('/galeri/tambah','System\GaleriController@store')->name('galeri.store');
        Route::post('/galeri/ubah', 'System\GaleriController@update')->name('galeri.update');
        Route::get('/galeri/hapus/{id}', 'System\GaleriController@destroy')->name('galeri.destroy');
        Route::get('/galeri/filter', 'System\GaleriController@filter')->name('galeri.filter');

        Route::get('/kategori-galeri', 'System\GaleriKategoriController@index')->name('kategori-galeri.index');
        Route::post('/kategori-galeri/tambah','System\GaleriKategoriController@store')->name('kategori-galeri.store');
        Route::post('/kategori-galeri/ubah', 'System\GaleriKategoriController@update')->name('kategori-galeri.update');
        Route::get('/kategori-galeri/hapus/{id}', 'System\GaleriKategoriController@destroy')->name('kategori-galeri.destroy');

        Route::get('/kategori-kegiatan', 'System\KegiatanKategoriController@index')->name('kategori-kegiatan.index');
        Route::post('/kategori-kegiatan/tambah','System\KegiatanKategoriController@store')->name('kategori-kegiatan.store');
        Route::post('/kategori-kegiatan/ubah', 'System\KegiatanKategoriController@update')->name('kategori-kegiatan.update');
        Route::get('/kategori-kegiatan/hapus/{id}', 'System\KegiatanKategoriController@destroy')->name('kategori-kegiatan.destroy');

        Route::get('/jamaah-aktif', 'System\JamaatController@index')->name('jamaah.index');
        Route::post('/jamaah-aktif/tambah','System\JamaatController@store')->name('jamaah.store');
        Route::post('/jamaah-aktif/ubah', 'System\JamaatController@update')->name('jamaah.update');
        Route::get('/jamaah-aktif/hapus/{id}', 'System\JamaatController@destroy')->name('jamaah.destroy');
        Route::get('/jamaah-aktif/cari', 'System\SearchController@cariJamaah')->name('jamaah.cari');

        Route::get('/kegiatan', 'System\KegiatanController@index')->name('kegiatan.index');
        Route::post('/kegiatan/tambah','System\KegiatanController@store')->name('kegiatan.store');
        Route::post('/kegiatan/ubah', 'System\KegiatanController@update')->name('kegiatan.update');
        Route::get('/kegiatan/hapus/{id}', 'System\KegiatanController@destroy')->name('kegiatan.destroy');
        Route::get('/kegiatan/cari', 'System\SearchController@cariKegiatan')->name('kegiatan.cari');
        Route::get('/kegiatan/filter', 'System\KegiatanController@filter')->name('kegiatan.filter');

        Route::get('/keuangan/penerimaan', 'System\KeuanganController@indexPenerimaan')->name('keuangan.penerimaan.index');
        Route::post('/keuangan/penerimaan/tambah','System\KeuanganController@store')->name('keuangan.penerimaan.store');
        Route::post('/keuangan/penerimaan/ubah', 'System\KeuanganController@update')->name('keuangan.penerimaan.update');
        Route::get('/keuangan/penerimaan/ekspor', 'System\EksporController@exportPenerimaan')->name('keuangan.penerimaan.ekspor');
        Route::get('/keuangan/penerimaan/hapus/{id}', 'System\KeuanganController@destroy')->name('keuangan.penerimaan.destroy');
        Route::get('/keuangan/penerimaan/cari', 'System\SearchController@cariKeuanganPenerimaan')->name('keuangan.penerimaan.cari');

        Route::get('/keuangan/pengeluaran', 'System\KeuanganController@indexPengeluaran')->name('keuangan.pengeluaran.index');
        Route::post('/keuangan/pengeluaran/tambah','System\KeuanganController@store')->name('keuangan.pengeluaran.store');
        Route::post('/keuangan/pengeluaran/ubah', 'System\KeuanganController@update')->name('keuangan.pengeluaran.update');
        Route::get('/keuangan/pengeluaran/ekspor', 'System\EksporController@exportPengeluaran')->name('keuangan.pengeluaran.ekspor');
        Route::get('/keuangan/pengeluaran/hapus/{id}', 'System\KeuanganController@destroy')->name('keuangan.pengeluaran.destroy');
        Route::get('/keuangan/pengeluaran/cari', 'System\SearchController@cariKeuanganPengeluaran')->name('keuangan.pengeluaran.cari');

        Route::get('/keuangan/zakat', 'System\KeuanganController@indexZakat')->name('keuangan.zakat.index');
        Route::post('/keuangan/zakat/tambah','System\KeuanganController@store')->name('keuangan.zakat.store');
        Route::post('/keuangan/zakat/ubah', 'System\KeuanganController@update')->name('keuangan.zakat.update');
        Route::get('/keuangan/zakat/ekspor', 'System\EksporController@exportZakat')->name('keuangan.zakat.ekspor');
        Route::get('/keuangan/zakat/hapus/{id}', 'System\KeuanganController@destroy')->name('keuangan.zakat.destroy');
        Route::get('/keuangan/zakat/cari', 'System\SearchController@cariKeuanganZakat')->name('keuangan.zakat.cari');

        Route::get('/keuangan/infaq', 'System\KeuanganController@indexInfaq')->name('keuangan.infaq.index');
        Route::post('/keuangan/infaq/tambah','System\KeuanganController@store')->name('keuangan.infaq.store');
        Route::post('/keuangan/infaq/ubah', 'System\KeuanganController@update')->name('keuangan.infaq.update');
        Route::get('/keuangan/infaq/ekspor', 'System\EksporController@exportInfaq')->name('keuangan.infaq.ekspor');
        Route::get('/keuangan/infaq/hapus/{id}', 'System\KeuanganController@destroy')->name('keuangan.infaq.destroy');
        Route::get('/keuangan/infaq/cari', 'System\SearchController@cariKeuanganInfaq')->name('keuangan.infaq.cari');

        Route::get('/pengurus', 'System\PengurusController@index')->name('pengurus.index');
        Route::post('/pengurus/tambah','System\PengurusController@store')->name('pengurus.store');
        Route::post('/pengurus/ubah', 'System\PengurusController@update')->name('pengurus.update');
        Route::get('/pengurus/hapus/{id}', 'System\PengurusController@destroy')->name('pengurus.destroy');
        Route::get('/pengurus/cari', 'System\SearchController@cariPengurus')->name('pengurus.cari');
    });
});