<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kegiatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kegiatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kategori_kegiatan_id')->unsigned();
            $table->foreign('kategori_kegiatan_id')->references('id')->on('kategori_kegiatan')->onDelete('cascade');
            $table->date('tgl_mulai');
            $table->date('tgl_selesai');
            $table->time('wkt_mulai');
            $table->time('wkt_selesai');
            $table->string('judul_kegiatan');
            $table->string('foto_kegiatan')->nullable();
            $table->longText('konten_kegiatan');
            $table->string('link_daftar_kegiatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kegiatan');
    }
}
