<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Carbon;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return [
            $this->admin(),
            $this->sekretaris(),
            $this->bendahara(),
        ];
    }

    private function admin()
    {
        $password = 'balikpapan2019';
        $encrypted = Crypt::encrypt($password);
        User::insert([
            'id' => '0',
            'name' => 'Administrator',
            'username' => 'admin',
            'role' => 'Admin',
            'password' => $encrypted,
            'created_at' => Carbon::now()
        ]);
    }

    private function sekretaris()
    {
        $password = 'sekretaris';
        $encrypted = Crypt::encrypt($password);
        User::insert([
            'id' => '0',
            'name' => 'Sekretaris',
            'username' => 'sekretaris',
            'role' => 'Sekretaris',
            'password' => $encrypted,
            'created_at' => Carbon::now()
        ]);
    }

    private function bendahara()
    {
        $password = 'bendahara';
        $encrypted = Crypt::encrypt($password);
        User::insert([
            'id' => '0',
            'name' => 'Bendahara',
            'username' => 'bendahara',
            'role' => 'Bendahara',
            'password' => $encrypted,
            'created_at' => Carbon::now()
        ]);
    }
}
