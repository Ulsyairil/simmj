@extends('website/layouts/app')

@section('title')
<title>Gallery | Masjid Jabalussu'ada Balikpapan</title>
@endsection

@section('content')
<div class="page-header header-filter" data-parallax="true"
    style="background-image: url({{ asset('img/default/12012.jpg') }})">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto">
                <div class="brand text-right">
                    <h1>Masjid Jabalussu'ada Balikpapan</h1>
                    <h3 class="title text-right">Gallery</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">
        <div class="section text-left">
            <h2 class="title">Gallery</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="col">
                    <form action="{{ route('gallery.filter') }}" method="GET">
                        <div class="input-group">
                            <select class="form-control" name="select_kategori" id="select_kategori" required>
                                <option value="">Pilih Kategori</option>
                                @foreach ($resultkategorigaleri as $data)
                                <option value="{{ $data->id }}">{{ $data->kategori }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <button type="submit" class="btn btn-info">Filter</button>
                                </span>
                                <span class="input-group-text">
                                    <a href="{{ route('gallery') }}">
                                        <button type="button" class="btn btn-info">Reset</button>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            @if (count($result) > 0)
            @foreach ($result as $key => $data)
            <div class="col-md-4 col-lg-3">
                <div class="card">
                    <a href="#galeri" data-toggle="modal" data-target="#galleryModal-{{ $key + $result->firstitem() }}">
                        <img class="card-img-top img-raised img-responsive" src="{{ asset($data->foto) }}"
                            alt="Card image cap" height="200">
                    </a>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-md-4 col-lg-3">
                <div class="card">
                    <img class="card-img-top img-raised img-responsive" src="{{ asset('img/default/no_preview.png') }}"
                        height="200" alt="Card image cap">
                </div>
            </div>
            @endif
        </div>
        {{ $result->links() }}
    </div>
</div>
@endsection

@section('modal')
@foreach ($result as $key => $data)
<div class="modal fade" id="galleryModal-{{ $key + $result->firstitem() }}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <img class="img-fluid img-responsive" src="{{ asset($data->foto) }}" alt="gallery">
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('body').loader('show');
        setTimeout(function () {
            $('body').loader('hide');
        }, 2000);
    });
</script>
@endsection