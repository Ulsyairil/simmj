@extends('website/layouts/app')

@section('title')
<title>About Us | Masjid Jabalussu'ada Balikpapan</title>
@endsection

@section('content')
<div class="page-header header-filter" data-parallax="true"
    style="background-image: url({{ asset('img/default/89502.jpg') }})">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto">
                <div class="brand text-right">
                    <h1>Masjid Jabalussu'ada Balikpapan</h1>
                    <h3 class="title text-right">About Us</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">
        <div class="section text-left">
            <h2 class="title">About Us</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-6 text-center">
                <img class="img-raised img-responsive" src="{{ asset('img/default/img_20160318_112444_hdr.jpg') }}" width="300" alt="about-us">
                <br><br>
            </div>
            <div class="col-md-6 text-justify">
                <span>Masjid yang berada di kompleks Perumahan 'Bukit Damai Indah'. Salah satu perumahan kelas menengah Balikpapan, berlokasi di Jl. MT Haryono. Dari pusat Kota berjarak sekitar 10 km arah ke Samarinda. Secara harfiah, jabal artinya bukit, su’ada artinya damai, bahagia. Jika digabung jadilah "Bukit Damai", kurang lebih sama dengan lokasi masjid berdiri "Bukit Damai Indah".</span>
            </div>
        </div>
        <br>
    </div>
    <div class="container">
        <div class="section text-center">
            <h2 class="title">Mosque Manager</h2>
            <hr>
        </div>
        <div class="row align-items-center justify-content-center">
            @if (count($result) > 0)
                @foreach ($result as $key => $data)
                    <div class="col-md-4 col-lg-3 col">
                        <div class="card">
                            <img class="card-img-top img-responsive" src="{{ $data->foto }}" width="200" alt="Card image cap">
                            <div class="card-body text-center text-center">
                                <h4 class="card-title">{{ $data->nama_lengkap }}</h4>
                                <h5 class="card-category">{{ $data->jabatan }}</h5>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <img class="card-img-top img-responsive" src="{{ asset('img/default/no_preview.png') }}" width="200" alt="Card image cap">
                        <div class="card-body text-center text-center">
                            <span class="card-title">No Data</span>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('body').loader('show');
        setTimeout(function () {
            $('body').loader('hide');
        }, 2000);
    });
</script>
@endsection