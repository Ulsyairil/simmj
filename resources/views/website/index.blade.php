@extends('website/layouts/app')

@section('title')
<title>Home | Masjid Jabalussu'ada Balikpapan</title>
@endsection

@section('content')
<style>
    .info {
        margin: 0 !important;
        padding: 0 !important;
    }
</style>
<div class="page-header header-filter" data-parallax="true"
    style="background-image: url({{ asset('img/default/383562-PBYIX2-695.jpg') }})">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto">
                <div class="brand text-right">
                    <h1>Masjid Jabalussu'ada Balikpapan</h1>
                    <h3 class="title text-right">Welcome to our website</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">
        <div class="section text-left">
            <h2 class="title">About Us</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-6 text-center">
                <img class="img-raised img-responsive" src="{{ asset('img/default/img_20160318_112444_hdr.jpg') }}" width="300" alt="about-us">
                <br><br>
            </div>
            <div class="col-md-6 text-justify">
                <span>Masjid yang berada di kompleks Perumahan 'Bukit Damai Indah'. Salah satu perumahan kelas menengah Balikpapan, berlokasi di Jl. MT Haryono. Dari pusat Kota berjarak sekitar 10 km arah ke Samarinda. Secara harfiah, jabal artinya bukit, su’ada artinya damai, bahagia. Jika digabung jadilah "Bukit Damai", kurang lebih sama dengan lokasi masjid berdiri "Bukit Damai Indah".</span>
                <br><br>
                <a href="{{ route('about-us') }}"><button type="button" class="btn btn-info">Read More</button></a>
            </div>
        </div>
        <br>
    </div>
    <div class="container">
        <div class="section text-center">
            <h2 class="title">Events</h2>
            <hr>
        </div>
        <div class="row">
            @if (count($resultEvent) > 0)
                @foreach ($resultEvent as $key => $data)
                    @if ($key <= 3)
                        <div class="col-md-4 col-lg-3">
                            <div class="card">
                                <img class="card-img-top img-responsive" src="{{ asset($data->foto_kegiatan) }}" alt="Card image cap" width="300">
                                <div class="card-body">
                                    <span class="card-title">{{ $data->judul_kegiatan }}</span>
                                </div>
                                <div class="card-footer">
                                    <a href="#event" data-toggle="modal" data-target="#eventsModal-{{ $key + $resultEvent->firstitem() }}"><button type="button"
                                            class="btn btn-info">Read More</button></a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @else
                <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <img class="card-img-top img-responsive" src="{{ asset('img/default/no_preview.png') }}" width="300" alt="Card image cap">
                        <div class="card-body">
                            <span class="card-title">No Data</span>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="text-center">
            <a href={{ route('event') }}><button type="button" class="btn btn-info">Look More</button></a>
        </div>
    </div>
    <div class="container">
        <div class="section text-center">
            <h2 class="title">Gallery</h2>
            <hr>
        </div>
        <div class="row">
            @if (count($resultGallery) > 0)
                @foreach ($resultGallery as $key => $data)
                    @if ($key <= 3)
                        <div class="col-md-4 col-lg-3">
                            <div class="card">
                                <a href="#galeri" data-toggle="modal" data-target="#galleryModal-{{ $key + $resultGallery->firstitem() }}">
                                    <img class="card-img-top img-raised img-responsive" src="{{ $data->foto }}" alt="Card image cap" height="200">
                                </a>
                            </div>
                        </div>
                    @endif
                @endforeach
            @else
                <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <img class="card-img-top img-raised img-responsive" height="200" src="{{ asset('img/default/no_preview.png') }}" alt="Card image cap">
                    </div>
                </div>
            @endif
        </div>
        <div class="text-center">
            <a href="{{ route('gallery') }}"><button type="button" class="btn btn-info">Look More</button></a>
        </div>
    </div>
    <div id="map"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="section-text">
                    <h2 class="title">Feedback</h2>
                    <hr>
                </div>
                <form action="{{ route('feedback') }}" method="POST" class="form">
                    @if (Session::has('alert-success'))
                    <div class="alert alert-success">
                        <div class="container">
                            <div class="alert-icon">
                                <i class="material-icons">check</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <p>{{ Session::get('alert-success') }}</p>
                        </div>
                    </div>
                    @elseif (Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <div class="container">
                            <div class="alert-icon">
                                <i class="material-icons">error_outline</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>
                            <p>{{ Session::get('alert-danger') }}</p>
                        </div>
                    </div>
                    @endif
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="input_name" id="input_name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="input_email" id="input_email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" name="input_subject" id="input_subject" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="input_message" id="input_message" class="form-control" cols="30"
                            rows="4"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info">Send</button>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="section-text">
                    <h2 class="title">Contact</h2>
                    <hr>
                </div>
                <div class="container">
                    <div class="info info-horizontal">
                        <div class="icon icon-info">
                            <i class="material-icons">pin_drop</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Address</h4>
                            <p> Gunung Bahagia, Balikpapan Selatan,<br>
                                Kota Balikpapan, Kalimantan Timur,<br>
                                76114
                            </p>
                        </div>
                    </div>
                    <div class="info info-horizontal">
                        <div class="icon icon-info">
                            <i class="material-icons">phone</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Phone Number</h4>
                            <p>- </p>
                        </div>
                    </div>
                    <div class="info info-horizontal">
                        <div class="icon icon-info">
                            <i class="material-icons">email</i>
                        </div>
                        <div class="description">
                            <h4 class="info-title">Email</h4>
                            <p><a href="mailto:info@masjidjabalussuada.com">info@masjidjabalussuada.com</a></p>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
@foreach ($resultEvent as $key => $data)
    <div class="modal fade" id="eventsModal-{{ $key + $resultEvent->firstitem() }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $data->judul_kegiatan }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="material-icons">clear</i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <img class="img-responsive" src="{{ asset($data->foto_kegiatan) }}" width="700" alt="gallery">
                    </div>
                    <br><br>
                    <table>
                        <tr>
                            <td><span>Tanggal</span></td>
                            <td><span> : </span></td>
                            <td><span>{{ date('d M Y', strtotime($data->tgl_mulai)) }} - 
                                @if ($data->tgl_mulai == $data->tgl_selesai)
                                    Selesai
                                @else
                                    {{ date('d M Y', strtotime($data->tgl_selesai)) }}
                                @endif
                            </span></td>
                        </tr>
                        <tr>
                            <td><span>Waktu</span></td>
                            <td><span> : </span></td>
                            <td><span>{{ date('H.i', strtotime($data->wkt_mulai)) }} - {{ date('H.i', strtotime($data->wkt_selesai)) }}</span></td>
                        </tr>
                        <tr>
                            <td><span>Link Daftar</span></td>
                            <td><span> : </span></td>
                            <td>
                                @if ($data->link_daftar_kegiatan == null)
                                    <span>-</span>
                                @else
                                    <span><a href="{{ $data->link_daftar_kegiatan }}" target="_blank">Click here</a></span>
                                @endif
                            </td>
                        </tr>
                    </table>
                    <br>
                    {!! $data->konten_kegiatan !!}
                </div>
            </div>
        </div>
    </div>
@endforeach
@foreach ($resultGallery as $key => $data)
    <div class="modal fade" id="galleryModal-{{ $key + $resultGallery->firstitem() }}" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img class="img-fluid img-responsive" src="{{ asset($data->foto) }}" alt="gallery">
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('script')
<script type="text/javascript">
    $(function () {
        function initMap() {
            var location = new google.maps.LatLng(-1.258228, 116.871402);
            var mapCanvas = document.getElementById('map');
            var mapOptions = {
                center: location,
                zoom: 17,
                scrollwheel: false,
                panControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            }
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var markerImage = 'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png';
            var marker = new google.maps.Marker({
                position: location,
                map: map,
                icon: markerImage
            });
            var contentString =
                '<div class="info-window">' +
                "<h3>Masjid Jabalussu'ada Balikpapan</h3>" +
                '<div class="info-content">' +
                "<p>Gunung Bahagia, Balikpapan Selatan, Kota Balikpapan, Kalimantan Timur, 76114"
            '</div>' +
            '</div>';
            var infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 400
            });
            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
        }
        google.maps.event.addDomListener(window, 'load', initMap);
    });
    $(document).ready(function () {
        $('body').loader('show');
        setTimeout(function () {
            $('body').loader('hide');
        }, 2000);
    });
</script>
@endsection
