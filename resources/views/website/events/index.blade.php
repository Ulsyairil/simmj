@extends('website/layouts/app')

@section('title')
<title>Events | Masjid Jabalussu'ada Balikpapan</title>
@endsection

@section('content')
<div class="page-header header-filter" data-parallax="true"
    style="background-image: url({{ asset('img/default/89328.jpg') }})">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto">
                <div class="brand text-right">
                    <h1>Masjid Jabalussu'ada Balikpapan</h1>
                    <h3 class="title text-right">Events</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main main-raised">
    <div class="container">
        <div class="section text-left">
            <h2 class="title">Events</h2>
            <hr>
        </div>
        <div class="row">
            <div class="col">
                <form action="{{ route('event.search') }}" class="form" method="POST">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <input type="text" class="form-control" name="input_cari" placeholder="Search" required>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <button type="submit" class="btn btn-round btn-info">
                                    <i class="material-icons">search</i>
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col">
                <form action="{{ route('event.filter') }}" method="GET">
                    <div class="input-group">
                        <select class="form-control" name="select_kategori" id="select_kategori" required>
                            <option value="">Pilih Kategori</option>
                            @foreach ($resultkategorikegiatan as $data)
                            <option value="{{ $data->id }}">{{ $data->kategori }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </span>
                            <span class="input-group-text">
                                <a href="{{ route('event') }}">
                                    <button type="button" class="btn btn-info">Reset</button>
                                </a>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            @if (count($result) > 0)
            @foreach ($result as $key => $data)
            <div class="col-md-4 col-lg-3">
                <div class="card">
                    <img class="card-img-top img-responsive" src="{{ asset($data->foto_kegiatan) }}"
                        alt="Card image cap" width="300">
                    <div class="card-body">
                        <span class="card-title">{{ $data->judul_kegiatan }}</span>
                    </div>
                    <div class="card-footer">
                        <a href="#event" data-toggle="modal"
                            data-target="#eventsModal-{{ $key + $result->firstitem() }}"><button type="button"
                                class="btn btn-info">Read More</button></a>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-md-4 col-lg-3">
                <div class="card">
                    <img class="card-img-top img-responsive" src="{{ asset('img/default/no_preview.png') }}" width="300"
                        alt="Card image cap">
                    <div class="card-body">
                        <span class="card-title">No Data</span>
                    </div>
                </div>
            </div>
            @endif
        </div>
        {{ $result->links() }}
    </div>
</div>
@endsection

@section('modal')
@foreach ($result as $key => $data)
<div class="modal fade" id="eventsModal-{{ $key + $result->firstitem() }}" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $data->judul_kegiatan }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img class="img-responsive" src="{{ asset($data->foto_kegiatan) }}" width="700" alt="gallery">
                </div>
                <br><br>
                <table>
                    <tr>
                        <td><span>Tanggal</span></td>
                        <td><span> : </span></td>
                        <td><span>{{ date('d M Y', strtotime($data->tgl_mulai)) }} -
                                @if ($data->tgl_mulai == $data->tgl_selesai)
                                Selesai
                                @else
                                {{ date('d M Y', strtotime($data->tgl_selesai)) }}
                                @endif
                            </span></td>
                    </tr>
                    <tr>
                        <td><span>Waktu</span></td>
                        <td><span> : </span></td>
                        <td><span>{{ date('H.i', strtotime($data->wkt_mulai)) }} -
                                {{ date('H.i', strtotime($data->wkt_selesai)) }}</span></td>
                    </tr>
                    <tr>
                        <td><span>Link Daftar</span></td>
                        <td><span> : </span></td>
                        <td>
                            @if ($data->link_daftar_kegiatan == null)
                            <span>-</span>
                            @else
                            <span><a href="{{ $data->link_daftar_kegiatan }}" target="_blank">Click here</a></span>
                            @endif
                        </td>
                    </tr>
                </table>
                <br>
                {!! $data->konten_kegiatan !!}
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('body').loader('show');
        setTimeout(function () {
            $('body').loader('hide');
        }, 2000);
    });

</script>
@endsection