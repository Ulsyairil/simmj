@extends('website/layouts/app')

@section('title')
<title>Login | Masjid Jabalussu'ada Balikpapan</title>
@endsection

@section('body-class')
class="login-page sidebar-collapse"
@endsection

@section('content')
<style>
    .card-body {
        margin-top: 100px;
    }
</style>
<div class="page-header header-filter"
    style="background-image: url({{ asset("img/default/383615-PBYJ13-351.jpg") }}); background-size: cover; background-position: top center;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                <div class="card card-login">
                    <form class="form" method="POST" action="{{ route('login.check') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-header card-header-info text-center">
                            <h4 class="card-title">Login</h4>
                        </div>
                        @if ($errors->any())
                        <div class="container">
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="material-icons">close</i>
                                </button>
                                @foreach ($errors->all() as $error)
                                <span>{{ $error }}</span>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        @if (Session::has('alert-success'))
                        <div class="container">
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <i class="material-icons">close</i>
                                </button>
                                <span>
                                    {{ Session::get('alert-success') }}
                                </span>
                            </div>
                        </div>
                        @endif
                        <div class="card-body">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">face</i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" name="email" placeholder="Username" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">lock_outline</i>
                                    </span>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="Password"
                                    required>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info btn-round">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('body').loader('show');
        setTimeout(function () {
            $('body').loader('hide');
        }, 2000);
    });
</script>
@endsection