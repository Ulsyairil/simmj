@section('footer')
<footer class="footer footer-default">
    <div class="container">
        <div class="copyright float-right">
            &copy;
            @php
                echo date('Y');
            @endphp Masjid Jabalussu'ada Balikpapan, Powered by
            <a href="https://www.creative-tim.com/" target="blank">Creative Tim</a>.
        </div>
    </div>
</footer>
@show
