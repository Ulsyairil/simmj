@section('navbar')
<nav class="navbar {{ Route::currentRouteNamed('login') ? 'navbar-transparent' : '' }} fixed-top navbar-expand-lg navbar-light bg-info" color-on-scroll="100">
    <div class="container">
        <div class="navbar-translate">
            @if (Route::currentRouteNamed('login'))
                <style>
                    .navbar-brand {
                        filter: brightness(0) invert(1) !important;
                    }
                </style>
            @endif
            <div class="navbar-brand">
                <img class="img-responsive" src="{{ asset('img/default/logo-lg.png') }}" width="100" alt="logo">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{ Route::currentRouteNamed('index') ? 'active' : '' }}">
                    <a href="{{ route('index') }}" class="nav-link">
                        <i class="material-icons">home</i> Home
                    </a>
                </li>
                <li class="nav-item {{ Route::currentRouteNamed('about-us') ? 'active' : '' }}">
                    <a href="{{ route('about-us') }}" class="nav-link">
                        <i class="material-icons">find_in_page</i> About Us
                    </a>
                </li>
                <li class="nav-item {{ Route::currentRouteNamed('event','event.search') ? 'active' : '' }}">
                    <a href="{{ route('event') }}" class="nav-link">
                        <i class="material-icons">event</i> Events
                    </a>
                </li>
                <li class="nav-item {{ Route::currentRouteNamed('gallery') ? 'active' : '' }}">
                    <a href="{{ route('gallery') }}" class="nav-link">
                        <i class="material-icons">image</i> Gallery
                    </a>
                </li>
                <li class="nav-item {{ Route::currentRouteNamed('login') ? 'active' : '' }}">
                    <a href="{{ route('login') }}" class="nav-link">
                        Login <i class="material-icons">chevron_right</i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
@show
