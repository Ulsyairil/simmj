@extends('system/layouts/app')

@section('title')
<title>Galeri | {{ env('APP_NAME') }}</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4 class="card-title">Data Galeri</h4>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span>
                        @endforeach
                    </div>
                    @endif
                    @if (Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-danger') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-success') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-warning') }}
                        </span>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-info" data-toggle="modal"
                                data-target="#galeriaddModal">Tambah</button>
                        </div>
                        <div class="col">
                            <form action="{{ route('galeri.filter') }}" method="GET">
                                <div class="input-group">
                                    <select class="form-control" name="select_kategori" id="select_kategori" required>
                                        <option value="">Pilih</option>
                                        @foreach ($resultkategorigaleri as $data)
                                            <option value="{{ $data->id }}">{{ $data->kategori }}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <button type="submit" class="btn btn-info">Filter</button>
                                        </span>
                                        <span class="input-group-text">
                                            <a href="{{ route('galeri.index') }}">
                                                <button type="button" class="btn btn-info">Reset</button>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="text-info">
                                <th class="text-center">No</th>
                                <th class="text-center">Foto</th>
                                <th class="text-center">Kategori</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @if (count($result) > 0)
                                @foreach ($result as $key => $data)
                                <tr>
                                    <td class="text-center">{{ $key + $result->firstitem() }}</td>
                                    <td class="text-center">
                                        <img src="{{ asset($data->foto) }}" width="100" alt="foto">
                                    </td>
                                    <td class="text-center">{{ $data->kategorigaleri->kategori }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-warning" data-toggle="modal"
                                            data-target="#galerieditModal-{{ $key + $result->firstitem() }}">Ubah</button>
                                        <a href="{{ route('galeri.destroy',$data->id) }}"><button
                                                class="btn btn-danger">Hapus</button></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-left" colspan="4">Tidak ada data</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $result->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="galeriaddModal" tabindex="-1" role="dialog" aria-labelledby="galeriaddModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="galeriaddModalLabel">Tambah Galeri</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('galeri.store') }}" role="form" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group text-center">
                        <img id="addgaleri" class="img-fluid img-responsive img-bordered"
                            src="{{ asset("img/default/no_preview.png") }}" width="200">
                        <br>
                        <label for="note">Catatan :</label>
                        <p>Ukuran File Upload Maksimal 2048 MB</p>
                    </div>
                    <div>
                        <label for="foto">Foto</label>
                        <input type="file" id="upload_add_galeri" name="input_foto" class="form-control"
                            onchange="addImage(this);" required>
                    </div>
                    <div class="form-group">
                        <label for="input_kategori">Kategori</label>
                        <select class="form-control" name="input_kategori" id="input_kategori" required>
                            <option value="">Pilih</option>
                            @foreach ($resultkategorigaleri as $data)
                            <option value="{{ $data->id }}">{{ $data->kategori }}</option>
                            @endforeach
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($result as $key => $data)
<div class="modal fade" id="galerieditModal-{{ $key + $result->firstitem() }}" tabindex="-1" role="dialog"
    aria-labelledby="galerieditModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="galerieditModalLabel">Ubah Galeri</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('galeri.update') }}" role="form" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="form-group text-center">
                        <img id="editgaleri" class="img-fluid img-responsive img-bordered"
                            src="{{ asset($data->foto) }}" width="200">
                        <br>
                        <label for="note">Catatan :</label>
                        <p>Ukuran File Upload Maksimal 2048 MB</p>
                    </div>
                    <div>
                        <label for="foto">Foto</label>
                        <input type="file" id="upload_add_galeri" name="input_foto" class="form-control"
                            onchange="editImage(this)">
                    </div>
                    <div class="form-group">
                        <label for="input_kategori">Kategori</label>
                        <select class="form-control" name="input_kategori" id="input_kategori" required>
                            <option value="">Pilih</option>
                            @php
                            $id = $data->kategori_galeri_id;
                            @endphp
                            @foreach ($resultkategorigaleri as $row)
                            <option value="{{ $row->id }}" 
                            @if ($row->id == $id)
                                selected
                                @endif
                                >{{ $row->kategori }}</option>
                            @endforeach
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('script')
<script type="text/javascript">
    function addImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#addgaleri')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function editImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#editgaleri')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection