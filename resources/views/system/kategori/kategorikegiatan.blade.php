@extends('system/layouts/app')

@section('title')
<title>Kategori Kegiatan | {{ env('APP_NAME') }}</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4 class="card-title">Kategori Kegiatan</h4>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                        @endforeach
                    </div>
                    @endif
                    @if (Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-danger') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-success') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-warning') }}
                        </span>
                    </div>
                    @endif
                    <button type="button" class="btn btn-info" data-toggle="modal"
                        data-target="#kegiatankegiatanaddModal">Tambah</button>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="text-info">
                                <th class="text-center">No</th>
                                <th class="text-center">Kategori</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @if (count($result) > 0)
                                @foreach ($result as $key => $data)
                                <tr>
                                    <td class="text-center">{{ $key + $result->firstitem() }}</td>
                                    <td class="text-center">{{ $data->kategori }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-warning" data-toggle="modal"
                                            data-target="#kegiatankegiataneditModal-{{ $key + $result->firstitem() }}">Ubah</button>
                                        <a href="{{ route('kategori-kegiatan.destroy',$data->id) }}"><button
                                                class="btn btn-danger">Hapus</button></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-left" colspan="3">Tidak ada data</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $result->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="kegiatankegiatanaddModal" tabindex="-1" role="dialog"
    aria-labelledby="kegiatankegiatanaddModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="kegiatankegiatanaddModalLabel">Tambah Kategori</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('kategori-kegiatan.store') }}" role="form" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="input_kategori">Kategori</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_kategori" id="input_kategori" required>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($result as $key => $data)
<div class="modal fade" id="kegiatankegiataneditModal-{{ $key + $result->firstitem() }}" tabindex="-1" role="dialog"
    aria-labelledby="kegiatankegiataneditModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="kegiatankegiataneditModalLabel">Ubah Kategori</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('kategori-kegiatan.update') }}" role="form" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="form-group">
                        <label for="input_kategori">Kategori</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_kategori" id="input_kategori"
                                value="{{ $data->kategori }}" required>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach