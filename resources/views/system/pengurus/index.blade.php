@extends('system/layouts/app')

@section('title')
<title>Pengurus Masjid | {{ env('APP_NAME') }}</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4 class="card-title">Data Pengurus Masjid</h4>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                        @endforeach
                    </div>
                    @endif
                    @if (Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-danger') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-success') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-warning') }}
                        </span>
                    </div>
                    @endif
                    <button type="button" class="btn btn-info" data-toggle="modal"
                        data-target="#pengurusaddModal">Tambah</button>
                    @if (Route::currentRouteNamed('pengurus.cari'))
                        <a href="{{ route('pengurus.index') }}"><button type="button" class="btn btn-info">Kembali</button></a>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="text-info">
                                <th class="text-center">No</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Foto</th>
                                <th class="text-center">Jabatan</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @if (count($result) > 0)
                                @foreach ($result as $key => $data)
                                <tr>
                                    <td class="text-center">{{ $key + $result->firstitem() }}</td>
                                    <td class="text-center">{{ $data->nama_lengkap }}</td>
                                    <td class="text-center">
                                        @if ($data->foto == null)
                                        <img src="{{ asset('img/default/no_preview.png') }}" alt="foto" width="100">
                                        @else
                                        <img src="{{ asset($data->foto) }}" alt="foto" width="100">
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $data->jabatan }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-warning" data-toggle="modal"
                                            data-target="#penguruseditModal-{{ $key + $result->firstitem() }}">Ubah</button>
                                        <a href="{{ route('pengurus.destroy',$data->id) }}"><button
                                                class="btn btn-danger">Hapus</button></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-left" colspan="5">Tidak ada data</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $result->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="pengurusaddModal" tabindex="-1" role="dialog" aria-labelledby="pengurusaddModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="pengurusaddModalLabel">Tambah Pengurus Masjid</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('pengurus.store') }}" role="form" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_nama" id="input_nama">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <img id="img" class="img-fluid img-responsive img-bordered"
                            src="{{ asset("img/default/no_preview.png") }}" width="200">
                        <br>
                        <label for="note">Catatan :</label>
                        <p>Ukuran File Upload Maksimal 2048 MB</p>
                    </div>
                    <label for="foto">Foto</label>
                    <input type="file" id="upload" name="foto" class="form-control" onchange="readURL(this)">
                    <br>
                    <div class="form-group">
                        <label for="jabatan">Jabatan</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_jabatan" id="input_jabatan" required>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($result as $key => $data)
<div class="modal fade" id="penguruseditModal-{{ $key + $result->firstitem() }}" tabindex="-1" role="dialog"
    aria-labelledby="penguruseditModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="penguruseditModalLabel">Tambah Pengurus Masjid</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('pengurus.update') }}" role="form" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_nama" id="input_nama"
                                value="{{ $data->nama_lengkap }}">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        @if ($data->foto == null)
                        <img id="img_edit_pengurus" class="img-fluid img-responsive img-bordered"
                            src="{{ asset("img/default/no_preview.png") }}" width="200">
                        @else
                        <img id="img_edit_pengurus" class="img-fluid img-responsive img-bordered"
                            src="{{ asset($data->foto) }}" width="200">
                        @endif
                        <br>
                        <label for="note">Catatan :</label>
                        <p>Ukuran File Upload Maksimal 2048 MB</p>
                    </div>
                    <label for="foto">Foto</label>
                    <input type="file" id="upload_edit_pengurus" name="foto" class="form-control"
                        onchange="readURL(this)" value="{{ $data->foto }}">
                    <br>
                    <div class="form-group">
                        <label for="jabatan">Jabatan</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_jabatan" id="input_jabatan"
                                value="{{ $data->jabatan }}">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('script')
<script type="text/javascript">
    $(function () {
        $('#upload').change(function () {
            var input = this;
            var url = $(this).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" ||
                    ext == "jpg")) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#img').attr('src', '{{ asset("img/default/no_preview.png") }}');
            }
        });
    });
    $(function () {
        $('#upload_edit_pengurus').change(function () {
            var input = this;
            var url = $(this).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" ||
                    ext == "jpg")) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img_edit_pengurus').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#img_edit_pengurus').attr('src', '{{ asset("img/default/no_preview.png") }}');
            }
        });
    });

</script>
@endsection
