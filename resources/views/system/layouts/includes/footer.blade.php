@section('footer')
<footer class="footer">
    <div class="container-fluid">
        <div class="copyright float-right">
            &copy;
            @php
                echo date('Y');
            @endphp Sistem Manajemen Masjid Jabalussu'ada Balikpapan, Powered by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
        </div>
    </div>
</footer>
@show
