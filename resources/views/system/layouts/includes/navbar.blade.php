@section('navbar')
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <p class="navbar-brand">Dashboard</p>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            @if (Route::currentRouteNamed('jamaah.*'))
            <form class="navbar-form" method="GET" action="{{ route('jamaah.cari') }}" enctype="multipart/form-data">
                <div class="input-group no-border">
                    <input type="text" name="input_cari" class="form-control" placeholder="Cari">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
            @elseif (Route::currentRouteNamed('kegiatan.*'))
            <form class="navbar-form" method="GET" action="{{ route('kegiatan.cari') }}" enctype="multipart/form-data">
                <div class="input-group no-border">
                    <input type="text" name="input_cari" class="form-control" placeholder="Cari">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
            @elseif (Route::currentRouteNamed('keuangan.penerimaan.*'))
            <form class="navbar-form" method="GET" action="{{ route('keuangan.penerimaan.cari') }}"
                enctype="multipart/form-data">
                <div class="input-group no-border">
                    <input type="text" name="input_cari" class="form-control" placeholder="Cari">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
            @elseif (Route::currentRouteNamed('keuangan.pengeluaran.*'))
            <form class="navbar-form" method="GET" action="{{ route('keuangan.pengeluaran.cari') }}"
                enctype="multipart/form-data">
                <div class="input-group no-border">
                    <input type="text" name="input_cari" class="form-control" placeholder="Cari">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
            @elseif (Route::currentRouteNamed('keuangan.zakat.*'))
            <form class="navbar-form" method="GET" action="{{ route('keuangan.zakat.cari') }}"
                enctype="multipart/form-data">
                <div class="input-group no-border">
                    <input type="text" name="input_cari" class="form-control" placeholder="Cari">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
            @elseif (Route::currentRouteNamed('keuangan.infaq.*'))
            <form class="navbar-form" method="GET" action="{{ route('keuangan.infaq.cari') }}"
                enctype="multipart/form-data">
                <div class="input-group no-border">
                    <input type="text" name="input_cari" class="form-control" placeholder="Cari">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
            @elseif (Route::currentRouteNamed('pengurus.*'))
            <form class="navbar-form" method="GET" action="{{ route('pengurus.cari') }}" enctype="multipart/form-data">
                <div class="input-group no-border">
                    <input type="text" name="input_cari" class="form-control" placeholder="Cari">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div>
            </form>
            @endif
            <ul class="navbar-nav">
                @if (Route::currentRouteNamed('keuangan.*'))
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#pablo" id="keuanganDropdown" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="material-icons">sort</i>
                        <p class="d-lg-none d-md-block">
                            Jenis Keuangan
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="keuanganDropdown">
                        <a href="{{ route('keuangan.penerimaan.index') }}" class="dropdown-item">
                            Penerimaan
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('keuangan.pengeluaran.index') }}" class="dropdown-item">
                            Pengeluaran
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('keuangan.zakat.index') }}" class="dropdown-item">
                            Zakat
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('keuangan.infaq.index') }}" class="dropdown-item">
                            Infaq
                        </a>
                    </div>
                </li>
                @endif
                <div class="dropdown-divider"></div>
            </ul>
        </div>
    </div>
</nav>
@show
