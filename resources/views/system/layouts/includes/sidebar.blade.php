@section('sidebar')
<div class="sidebar" data-color="azure" data-background-color="white"
    data-image="{{ asset('img/default/img_20160318_112444_hdr.jpg') }}">
    <div class="logo text-center">
        <img src="{{ asset('img/default/logo-lg.png') }}" width="200" alt="logo">
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            @if (Auth::check() && Auth::user()->role == "Admin")
            <li class="nav-item {{ Route::currentRouteNamed('dashboard.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.index') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('kategori-galeri.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('kategori-galeri.index') }}">
                    <i class="material-icons">perm_media</i>
                    <p>Kategori Galeri</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('galeri.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('galeri.index') }}">
                    <i class="material-icons">perm_media</i>
                    <p>Data Galeri</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('jamaah.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('jamaah.index') }}">
                    <i class="material-icons">verified_user</i>
                    <p>Data Jamaah Aktif</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('kategori-kegiatan.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('kategori-kegiatan.index') }}">
                    <i class="material-icons">event</i>
                    <p>Kategori Kegiatan</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('kegiatan.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('kegiatan.index') }}">
                    <i class="material-icons">event</i>
                    <p>Data Kegiatan</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('keuangan.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('keuangan.penerimaan.index') }}">
                    <i class="material-icons">store</i>
                    <p>Data Keuangan</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('pengurus.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('pengurus.index') }}">
                    <i class='material-icons'>accessibility</i>
                    <p>Data Pengurus Masjid</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('pengguna.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('pengguna.edit',1) }}">
                    <i class='material-icons'>account_box</i>
                    <p>Ubah Profil</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('logout') ? 'active' : '' }}">
                <a class="nav-link" href="#logout" data-toggle="modal" data-target="#logoutModal">
                    <i class='material-icons'>power_settings_new</i>
                    <p>Logout</p>
                </a>
            </li>
            @elseif (Auth::check() && Auth::user()->role == "Sekretaris")
            <li class="nav-item {{ Route::currentRouteNamed('dashboard.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.index') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('kategori-galeri.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('kategori-galeri.index') }}">
                    <i class="material-icons">perm_media</i>
                    <p>Kategori Galeri</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('galeri.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('galeri.index') }}">
                    <i class="material-icons">perm_media</i>
                    <p>Data Galeri</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('jamaah.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('jamaah.index') }}">
                    <i class="material-icons">verified_user</i>
                    <p>Data Jamaah Aktif</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('kategori-kegiatan.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('kategori-kegiatan.index') }}">
                    <i class="material-icons">event</i>
                    <p>Kategori Kegiatan</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('kegiatan.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('kegiatan.index') }}">
                    <i class="material-icons">event</i>
                    <p>Data Kegiatan</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('pengguna.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('pengguna.edit',2) }}">
                    <i class='material-icons'>account_box</i>
                    <p>Ubah Profil</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('logout') ? 'active' : '' }}">
                <a class="nav-link" href="#logout" data-toggle="modal" data-target="#logoutModal">
                    <i class='material-icons'>power_settings_new</i>
                    <p>Logout</p>
                </a>
            </li>
            @elseif (Auth::check() && Auth::user()->role == 'Bendahara')
            <li class="nav-item {{ Route::currentRouteNamed('dashboard.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.index') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('keuangan.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('keuangan.penerimaan.index') }}">
                    <i class="material-icons">store</i>
                    <p>Data Keuangan</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('pengguna.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('pengguna.edit',3) }}">
                    <i class='material-icons'>account_box</i>
                    <p>Ubah Profil</p>
                </a>
            </li>
            <li class="nav-item {{ Route::currentRouteNamed('logout') ? 'active' : '' }}">
                <a class="nav-link" href="#logout" data-toggle="modal" data-target="#logoutModal">
                    <i class='material-icons'>power_settings_new</i>
                    <p>Logout</p>
                </a>
            </li>
            @endif
        </ul>
    </div>
</div>
@show
