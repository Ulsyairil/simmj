<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport'>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('title')
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/material-dashboard.min.css?v=2.1.1') }}">
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.1/css/froala_editor.pkgd.min.css" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">
    <style>
    .content-mobile {
        display: none ;
    }
    
    @media screen and (max-width: 1199px) {
        .content-mobile {
            display: block;
        }
    }
    </style>
</head>

<body>
    <div class="wrapper">
        @include('system/layouts/includes/sidebar')
        <div class="main-panel">
            @include('system/layouts/includes/navbar')
            <div class="content">
                @yield('content')
            </div>
            @include('system/layouts/includes/footer')
        </div>
        @yield('modal')
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
                    </div>
                    <div class="modal-body">
                        Logout Sekarang ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
                        <a href="{{ route('logout') }}">
                            <button type="button" class="btn btn-danger">Logout</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Scipt --}}
    <script src="{{ asset('js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('js/core/popper.min.js') }}"></script>
    <script src="{{ asset('js/core/bootstrap-material-design.min.js') }}"></script>
    <script src="{{ asset('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('js/plugins/moment.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.bootstrap-wizard.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-selectpicker.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('js/plugins/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-jvectormap.js') }}"></script>
    <script src="{{ asset('js/plugins/nouislider.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <script src="{{ asset('js/plugins/arrive.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgzruFTTvea0LEmw_jAqknqskKDuJK7dM"></script>
    <script src="{{ asset('js/plugins/chartist.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
    <script src="{{ asset('js/plugins/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('js/material-dashboard.min.js?v=2.1.1') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.0.1/js/froala_editor.pkgd.min.js">
    </script>
    @yield('script')
    <script type="text/javascript">
        $(document).ready(function () {
            //init DateTimePickers
            md.initFormExtendedDatetimepickers();
        });
    </script>
</body>

</html>
