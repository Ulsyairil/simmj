@extends('system/layouts/app')

@section('title')
<title>Keuangan | {{ env('APP_NAME') }}</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">Jumlah Keuangan :</h5>
                            <span>Rp. {{ $resultmoney }}</span>
                        </div>
                        <div class="col-md-6">
                            <h5 class="card-title">Jumlah Pengeluaran :</h5>
                            <span>Rp. {{ $resultpengeluaran }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4 class="card-title">Data Keuangan : Pengeluaran</h4>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                        @endforeach
                    </div>
                    @endif
                    @if (Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-danger') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-success') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-warning') }}
                        </span>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-info" data-toggle="modal"
                                data-target="#keuanganaddModal">Tambah</button>
                            @if (Route::currentRouteNamed('keuangan.pengeluaran.cari'))
                            <a href="{{ URL::previous() }}"><button type="button"
                                    class="btn btn-info">Kembali</button></a>
                            @endif
                        </div>
                        <div class="col">
                            <form action="{{ route('keuangan.pengeluaran.ekspor') }}" method="GET">
                                <div class="input-group">
                                    <select class="form-control" name="select_bulan" id="select_bulan" required>
                                        <option value="">Pilih Bulan</option>
                                        @for ($i = 0; $i < 12; $i++)
                                            @php
                                                $time = strtotime(sprintf('%d months', $i));
                                                $label = date('F',$time);   
                                                $value = date('n',$time);
                                            @endphp
                                            <option value="{{ $value }}" >{{ $label }}</option>
                                        @endfor
                                    </select>
                                    <select class="form-control" name="select_tahun" id="select_tahun" required>
                                        <option value="">Pilih Tahun</option>
                                        @php
                                            $current = date('Y');
                                            $earliest = 1990;
                                            $latest = date('Y');
                                        @endphp
                                        @foreach (range($latest, $earliest) as $year)
                                            <option value="{{ $year }}">{{ $year }}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <button class="btn btn-info" type="submit">
                                                <i class='material-icons'>import_export</i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="text-info">
                                <th class="text-center">No</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Jumlah</th>
                                <th class="text-center">Keterangan</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @if (count($result) > 0)
                                @foreach ($result as $key => $data)
                                <tr>
                                    <td class="text-center">{{ $key + $result->firstitem() }}</td>
                                    <td class="text-center">{{ date('d F Y', strtotime($data->tgl)) }}</td>
                                    <td class="text-center">Rp. {{ number_format($data->jumlah,0,",",".") }}</td>
                                    <td>{{ $data->keterangan }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-warning" data-toggle="modal"
                                            data-target="#keuanganeditModal-{{ $key + $result->firstitem() }}">Ubah</button>
                                        <a href="{{ route('keuangan.pengeluaran.destroy',$data->id) }}"><button
                                                class="btn btn-danger">Hapus</button></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-left" colspan="5">Tidak ada data</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $result->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="keuanganaddModal" tabindex="-1" role="dialog" aria-labelledby="keuanganaddModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="keuanganaddModalLabel">Tambah Data Keuangan</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('keuangan.pengeluaran.store') }}" role="form" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <div class="input-group">
                            <input class="form-control" type="date" name="input_tanggal" id="input_tanggal" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jenis">Jenis</label>
                        <div class="input-group">
                            <select class="form-control" name="input_jenis" id="input_jenis">
                                <option value="Penerimaan">Penerimaan</option>
                                <option value="Pengeluaran" selected>Pengeluaran</option>
                                <option value="Zakat">Zakat</option>
                                <option value="Infaq">Infaq</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <div class="input-group">
                            <input type="text" id="onlyNumbers" class="form-control" name="input_jumlah" min="0"
                                onkeypress="allowNumbersOnly(event)" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <div class="input-group">
                            <textarea class="form-control" name="textarea_keterangan" id="textarea_keterangan" cols="30"
                                rows="5"></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($result as $key => $data)
<div class="modal fade" id="keuanganeditModal-{{ $key + $result->firstitem() }}" tabindex="-1" role="dialog"
    aria-labelledby="keuanganeditModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="keuanganeditModalLabel">Ubah Data Keuangan</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('keuangan.pengeluaran.update') }}" role="form" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <div class="input-group">
                            <input class="form-control" type="date" name="input_tanggal" id="input_tanggal"
                                value="{{ $data->tgl }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jenis">Jenis</label>
                        <div class="input-group">
                            <select class="form-control" name="input_jenis" id="input_jenis">
                                @if ($data->jenis == "Penerimaan")
                                <option value="Penerimaan" selected>Penerimaan</option>
                                <option value="Pengeluaran">Pengeluaran</option>
                                <option value="Zakat">Zakat</option>
                                <option value="Infaq">Infaq</option>
                                @elseif ($data->jenis == "Pengeluaran")
                                <option value="Penerimaan">Penerimaan</option>
                                <option value="Pengeluaran" selected>Pengeluaran</option>
                                <option value="Zakat">Zakat</option>
                                <option value="Infaq">Infaq</option>
                                @elseif ($data->jenis == "Zakat")
                                <option value="Penerimaan">Penerimaan</option>
                                <option value="Pengeluaran">Pengeluaran</option>
                                <option value="Zakat" selected>Zakat</option>
                                <option value="Infaq">Infaq</option>
                                @else
                                <option value="Penerimaan">Penerimaan</option>
                                <option value="Pengeluaran">Pengeluaran</option>
                                <option value="Zakat">Zakat</option>
                                <option value="Infaq" selected>Infaq</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <div class="input-group">
                            <input type="text" id="onlyNumbers" class="form-control" name="input_jumlah" min="0"
                                onkeypress="allowNumbersOnly(event)" value="{{ $data->jumlah }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <div class="input-group">
                            <textarea class="form-control" name="textarea_keterangan" id="textarea_keterangan" cols="30"
                                rows="5">{{ $data->keterangan }}</textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('script')
<script type="text/javascript">
    function allowNumbersOnly(e) {
        var code = (e.which) ? e.which : e.keyCode;
        if (code > 31 && (code < 48 || code > 57)) {
            e.preventDefault();
        }
    }

</script>
@endsection
