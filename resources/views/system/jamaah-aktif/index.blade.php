@extends('system/layouts/app')

@section('title')
<title>Jamaah Aktif | {{ env('APP_NAME') }}</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4 class="card-title">Data Jamaah Aktif</h4>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                        @endforeach
                    </div>
                    @endif
                    @if (Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-danger') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-success') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-warning') }}
                        </span>
                    </div>
                    @endif
                    <button type="button" class="btn btn-info" data-toggle="modal"
                        data-target="#jamaahaktifaddModal">Tambah</button>
                    @if (Route::currentRouteNamed('jamaah.cari'))
                        <a href="{{ URL::previous() }}"><button type="button" class="btn btn-info">Kembali</button></a>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="text-info">
                                <th class="text-center">No</th>
                                <th class="text-center">Nama Lengkap</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @if (count($result) > 0)
                                @foreach ($result as $key => $data)
                                <tr>
                                    <td class="text-center">{{ $key + $result->firstitem() }}</td>
                                    <td class="text-center">{{ $data->nama_lengkap }}</td>
                                    <td class="text-center">{{ $data->status }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-warning" data-toggle="modal"
                                            data-target="#jamaahaktifeditModal-{{ $key + $result->firstitem() }}">Ubah</button>
                                        <a href="{{ route('jamaah.destroy',$data->id) }}"><button
                                                class="btn btn-danger">Hapus</button></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-left" colspan="4">Tidak ada data</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $result->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="jamaahaktifaddModal" tabindex="-1" role="dialog" aria-labelledby="jamaahaktifaddModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="jamaahaktifaddModalLabel">Tambah Jamaah Aktif</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('jamaah.store') }}" role="form" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="nama_lengkap">Nama Lengkap</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_nama_lengkap" id="input_nama_lengkap"
                                required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <div class="input-group">
                            <textarea class="form-control" name="textarea_keterangan" id="textarea_keterangan" cols="30"
                                rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="input-group">
                            <select class="form-control" name="select_status" id="select_status" required>
                                <option value="Aktif">Aktif</option>
                                <option value="Non-Aktif" selected>Non-Aktif</option>
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($result as $key => $data)
<div class="modal fade" id="jamaahaktifeditModal-{{ $key + $result->firstitem() }}" tabindex="-1" role="dialog"
    aria-labelledby="jamaahaktifeditModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="jamaahaktifeditModalLabel">Ubah Jamaah Aktif</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('jamaah.update') }}" role="form" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="form-group">
                        <label for="nama_lengkap">Nama Lengkap</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_nama_lengkap" id="input_nama_lengkap"
                                value="{{ $data->nama_lengkap }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <div class="input-group">
                            <textarea class="form-control" name="textarea_keterangan" id="textarea_keterangan" cols="30"
                                rows="5">{{ $data->keterangan }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <div class="input-group">
                            <select class="form-control" name="select_status" id="select_status" required>
                                @if ($data->status == 'Aktif')
                                <option value="Aktif" selected>Aktif</option>
                                <option value="Non-Aktif">Non-Aktif</option>
                                @else
                                <option value="Aktif">Aktif</option>
                                <option value="Non-Aktif" selected>Non-Aktif</option>
                                @endif
                            </select>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection
