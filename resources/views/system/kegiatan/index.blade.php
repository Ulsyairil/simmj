@extends('system/layouts/app')

@section('title')
<title>Kegiatan | {{ env('APP_NAME') }}</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4 class="card-title">Kegiatan</h4>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        @foreach ($errors->all() as $error)
                        <span>{{ $error }}</span>
                        @endforeach
                    </div>
                    @endif
                    @if (Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-danger') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-success') }}
                        </span>
                    </div>
                    @elseif (Session::has('alert-warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>
                            {{ Session::get('alert-warning') }}
                        </span>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-info" data-toggle="modal"
                                data-target="#kegiatanaddModal">Tambah</button>
                        </div>
                        <div class="col">
                            <form action="{{ route('kegiatan.filter') }}" method="GET">
                                <div class="input-group">
                                    <select class="form-control" name="select_kategori" id="select_kategori" required>
                                        <option value="">Pilih</option>
                                        @foreach ($resultkategorikegiatan as $data)
                                        <option value="{{ $data->id }}">{{ $data->kategori }}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <button type="submit" class="btn btn-info">Filter</button>
                                        </span>
                                        <span class="input-group-text">
                                            <a href="{{ route('kegiatan.index') }}">
                                                <button type="button" class="btn btn-info">Reset</button>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="text-info">
                                <th class="text-center">No</th>
                                <th class="text-center">Kategori</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Waktu</th>
                                <th class="text-center">Judul</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @if (count($result) > 0)
                                @foreach ($result as $key => $data)
                                @php
                                $format_tgl_mulai = date("d F Y", strtotime($data->tgl_mulai));
                                $format_tgl_selesai = date("d F Y", strtotime($data->tgl_selesai));
                                $format_wkt_mulai = date("H:i", strtotime($data->wkt_mulai));
                                $format_wkt_selesai = date("H:i", strtotime($data->wkt_selesai));
                                @endphp
                                <tr>
                                    <td class="text-center">{{ $key + $result->firstitem() }}</td>
                                    <td class="text-center">{{ $data->kategorikegiatan->kategori }}</td>
                                    <td class="text-center">{{ $format_tgl_mulai }} -
                                        @if ($format_tgl_mulai == $format_tgl_selesai)
                                        Selesai
                                        @else
                                        {{ $format_tgl_selesai }}
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $format_wkt_mulai }} -
                                        @if ($format_wkt_mulai == $format_wkt_selesai)
                                        Selesai
                                        @else
                                        {{ $format_wkt_selesai }}
                                        @endif
                                    </td>
                                    <td>{{ $data->judul_kegiatan }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-warning" data-toggle="modal"
                                            data-target="#kegiataneditModal-{{ $key + $result->firstitem() }}">Ubah</button>
                                        <a href="{{ route('kegiatan.destroy',$data->id) }}"><button
                                                class="btn btn-danger">Hapus</button></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td class="text-left" colspan="6">Tidak ada data</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $result->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="kegiatanaddModal" tabindex="-1" role="dialog" aria-labelledby="kegiatanaddModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="kegiatanaddModalLabel">Tambah Kegiatan</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('kegiatan.store') }}" role="form" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tanggal_mulai">Tanggal Mulai</label>
                                <div class="input-group">
                                    <input class="form-control" type="date" name="input_tanggal_mulai"
                                        id="input_tanggal_mulai" required>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tanggal_selesai">Tanggal Selesai</label>
                                <div class="input-group">
                                    <input class="form-control" type="date" name="input_tanggal_selesai"
                                        id="input_tanggal_selesai" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="waktu_mulai">Waktu Mulai</label>
                                <div class="input-group">
                                    <input class="form-control" type="time" name="input_waktu_mulai"
                                        id="input_waktu_mulai" required>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="waktu_selesai">Waktu Selesai</label>
                                <div class="input-group">
                                    <input class="form-control" type="time" name="input_waktu_selesai"
                                        id="input_waktu_selesai" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_judul" id="input_judul" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input_kategori">Kategori</label>
                        <select class="form-control" name="input_kategori" id="input_kategori" required>
                            <option value="">Pilih</option>
                            @foreach ($resultkategorikegiatan as $data)
                            <option value="{{ $data->id }}">{{ $data->kategori }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group text-center">
                        <img id="img_add_kegiatan" class="img-fluid img-responsive img-bordered"
                            src="{{ asset("img/default/no_preview.png") }}" width="200">
                        <br>
                        <label for="note">Catatan :</label>
                        <p>Ukuran File Upload Maksimal 2048 MB</p>
                    </div>
                    <label for="foto">Foto</label>
                    <div class="input-group">
                        <input type="file" id="upload_add_kegiatan" name="foto" class="form-control"
                            onchange="readURL(this)">
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="konten">Konten</label>
                        <br>
                        <div class="input-group">
                            <textarea class="form-control" name="textarea_konten" id="textarea_konten" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="link_daftar_kegiatan">Link Daftar</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="input_link_daftar" id="input_link_daftar">
                        </div>
                        <span>Dapat menggunakan google docs : <a
                                href="https://docs.google.com/">https://docs.google.com/</a></span>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@foreach ($result as $key => $data)
<div class="modal fade" id="kegiataneditModal-{{ $key + $result->firstitem() }}" tabindex="-1" role="dialog"
    aria-labelledby="kegiataneditModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="kegiataneditModalLabel">Ubah Kegiatan</h5>
            </div>
            <div class="modal-body">
                <form action="{{ route('kegiatan.update') }}" role="form" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="tanggal_mulai">Tanggal Mulai</label>
                                <div class="input-group">
                                    <input class="form-control" type="date" name="input_tanggal_mulai"
                                        id="input_tanggal_mulai" value="{{ $data->tgl_mulai }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="tanggal_selesai">Tanggal Selesai</label>
                                <div class="input-group">
                                    <input class="form-control" type="date" name="input_tanggal_selesai"
                                        id="input_tanggal_selesai" value="{{ $data->tgl_selesai }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="waktu_mulai">Waktu Mulai</label>
                                <div class="input-group">
                                    <input class="form-control" type="time" name="input_waktu_mulai"
                                        id="input_waktu_mulai" value="{{ $data->wkt_mulai }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="waktu_selesai">Waktu Selesai</label>
                                <div class="input-group">
                                    <input class="form-control" type="time" name="input_waktu_selesai"
                                        id="input_waktu_selesai" value="{{ $data->wkt_selesai }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="input_judul" id="input_judul"
                                value="{{ $data->judul_kegiatan }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input_kategori">Kategori</label>
                        <select class="form-control" name="input_kategori" id="input_kategori" required>
                            <option value="">Pilih</option>
                            @php
                            $id = $data->kategori_kegiatan_id;
                            @endphp
                            @foreach ($resultkategorikegiatan as $row)
                            <option value="{{ $row->id }}" 
                                @if ($row->id == $id)
                                    selected
                                @endif
                                >{{ $row->kategori }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group text-center">
                        @if ($data->foto_kegiatan == null)
                        <img id="img_edit_kegiatan" class="img-fluid img-responsive img-bordered"
                            src="{{ asset("img/default/no_preview.png") }}" width="200">
                        @else
                        <img id="img_edit_kegiatan" class="img-fluid img-responsive img-bordered"
                            src="{{ asset($data->foto_kegiatan) }}" width="200">
                        @endif
                        <br>
                        <label for="note">Catatan :</label>
                        <p>Ukuran File Upload Maksimal 2048 MB</p>
                    </div>
                    <label for="foto">Foto</label>
                    <div class="input-group">
                        <input type="file" id="upload_edit_kegiatan" name="foto" class="form-control"
                            value="{{ $data->foto_kegiatan }}" onchange="readURL(this)">
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="konten">Konten</label>
                        <br>
                        <div class="input-group">
                            <textarea class="form-control" name="textarea_konten" id="textarea_konten">{{ $data->konten_kegiatan }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="link_daftar">Link Daftar</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="input_link_daftar" id="input_link_daftar"
                                value="{{ $data->link_daftar_kegiatan }}">
                        </div>
                        <span>Dapat menggunakan google docs : <a
                                href="https://docs.google.com/">https://docs.google.com/</a></span>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Kembali</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@endsection

@section('script')
<script>
    new FroalaEditor('textarea#textarea_konten');
    $(function () {
        $('#upload_add_kegiatan').change(function () {
            var input = this;
            var url = $(this).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" ||
                    ext == "jpg")) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img_add_kegiatan').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#img_add_kegiatan').attr('src', '{{ asset("img/default/no_preview.png") }}');
            }
        });
    });
    $(function () {
        $('#upload_edit_kegiatan').change(function () {
            var input = this;
            var url = $(this).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" ||
                    ext == "jpg")) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img_edit_kegiatan').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#img_edit_kegiatan').attr('src', '{{ asset("img/default/no_preview.png") }}');
            }
        });
    });

</script>
@endsection