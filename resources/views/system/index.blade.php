@extends('system/layouts/app')

@section('title')
<title>Dasboard | {{ env('APP_NAME') }}</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Selamat Datang {{ Auth::user()->name }}</h4>
            <p class="card-category">Sistem Informasi Manajemen Masjid Jabalussu'ada Balikpapan</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">store</i>
                    </div>
                    <p class="card-category">Keuangan</p>
                    <span class="card-title">Rp. {{ $hasilkeuangan }}</span>
                </div>
                <div class="card-footer">
                    @if (Auth::check() && Auth::user()->role == ["Admin","Keuangan"])
                    <div class="stats">
                        <a href="{{ route('keuangan.penerimaan.index') }}">Selengkapnya</a>
                        <i class="material-icons">arrow_right_alt</i>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">event</i>
                    </div>
                    <p class="card-category">Kegiatan</p>
                    <span class="card-title">{{ $kegiatan }}</span>
                </div>
                <div class="card-footer">
                    @if (Auth::check() && Auth::user()->role == ["Admin","Sekretaris"])
                    <div class="stats">
                        <a href="{{ route('kegiatan.index') }}">Selengkapnya</a>
                        <i class="material-icons">arrow_right_alt</i>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">perm_media</i>
                    </div>
                    <p class="card-category">Galeri</p>
                    <span class="card-title">{{ $galeri }}</span>
                </div>
                <div class="card-footer">
                    @if (Auth::check() && Auth::user()->role == ["Admin"])
                    <div class="stats">
                        <a href="{{ route('galeri.index') }}">Selengkapnya</a>
                        <i class="material-icons">arrow_right_alt</i>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">verified_user</i>
                    </div>
                    <p class="card-category">Jamaah Aktif</p>
                    <span class="card-title">{{ $jamaahaktif }}</span>
                </div>
                <div class="card-footer">
                    @if (Auth::check() && Auth::user()->role == ["Admin","Sekretaris"])
                    <div class="stats">
                        <a href="{{ route('jamaah.index') }}">Selengkapnya</a>
                        <i class="material-icons">arrow_right_alt</i>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-chart">
                <div class="card-header card-header-success">
                    <h4 class="card-title">Keuangan : Penerimaan</h4>
                </div>
                <div class="card-body">
                    <canvas id="penerimaan"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-chart">
                <div class="card-header card-header-success">
                    <h4 class="card-title">Keuangan : Pengeluaran</h4>
                </div>
                <div class="card-body">
                    <canvas id="pengeluaran"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-chart">
                <div class="card-header card-header-success">
                    <h4 class="card-title">Keuangan : Zakat</h4>
                </div>
                <div class="card-body">
                    <canvas id="zakat"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-chart">
                <div class="card-header card-header-success">
                    <h4 class="card-title">Keuangan : Infaq</h4>
                </div>
                <div class="card-body">
                    <canvas id="infaq"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        var url = "{{url('dashboard/chart/penerimaan')}}";
        var tgl = new Array();
        var jns = new Array();
        var jmlh = new Array();
        $.get(url, function (response) {
            response.forEach(function (data) {
                tgl.push(data.tgl);
                jns.push(data.jenis);
                jmlh.push(data.jumlah);
            });
            var ctx = document.getElementById("penerimaan").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: tgl,
                    datasets: [{
                        label: 'Penerimaan',
                        data: jmlh,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
    });

    $(document).ready(function () {
        var url = "{{url('dashboard/chart/pengeluaran')}}";
        var tgl = new Array();
        var jns = new Array();
        var jmlh = new Array();
        $.get(url, function (response) {
            response.forEach(function (data) {
                tgl.push(data.tgl);
                jns.push(data.jenis);
                jmlh.push(data.jumlah);
            });
            var ctx = document.getElementById("pengeluaran").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: tgl,
                    datasets: [{
                        label: 'Pengeluaran',
                        data: jmlh,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
    });

    $(document).ready(function () {
        var url = "{{url('dashboard/chart/zakat')}}";
        var tgl = new Array();
        var jns = new Array();
        var jmlh = new Array();
        $.get(url, function (response) {
            response.forEach(function (data) {
                tgl.push(data.tgl);
                jns.push(data.jenis);
                jmlh.push(data.jumlah);
            });
            var ctx = document.getElementById("zakat").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: tgl,
                    datasets: [{
                        label: 'Zakat',
                        data: jmlh,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
    });

    $(document).ready(function () {
        var url = "{{url('dashboard/chart/infaq')}}";
        var tgl = new Array();
        var jns = new Array();
        var jmlh = new Array();
        $.get(url, function (response) {
            response.forEach(function (data) {
                tgl.push(data.tgl);
                jns.push(data.jenis);
                jmlh.push(data.jumlah);
            });
            var ctx = document.getElementById("infaq").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: tgl,
                    datasets: [{
                        label: 'Infaq',
                        data: jmlh,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        });
    });

</script>
@endsection
