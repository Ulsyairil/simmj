<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use App\Model\Keuangan;

class PengeluaranKeuanganExport implements FromCollection, ShouldAutoSize, WithHeadings, WithDrawings
{
    public function __construct(string $a, string $b)
    {
        $this->bulan = $a;
        $this->tahun = $b;
    }

    private function countallmoney() 
    {
        $penerimaan = Keuangan::where('jenis','Penerimaan')
                ->sum('jumlah');
        $pengeluaran = Keuangan::where('jenis','Pengeluaran')
                ->sum('jumlah');
        $zakat = Keuangan::where('jenis','Zakat')
                ->sum('jumlah');
        $infaq = Keuangan::where('jenis','Infaq')
                ->sum('jumlah');
        $hitung = $penerimaan + $zakat + $infaq - $pengeluaran;
        return $hitung;
    }

    private function countmoney()
    {
        $money =Keuangan::where('jenis','Pengeluaran')
                ->whereMonth('tgl',$this->bulan)
                ->whereYear('tgl',$this->tahun)
                ->orderBy('tgl','desc')
                ->sum('jumlah');
        return $money;
    }

    public function drawings()
    {
        $drawing = new Drawing;
        $drawing->setName('Masjid Jabalussuada Balikpapan');
        $drawing->setPath(\public_path('/img/default/logo-lg.png'));
        $drawing->setHeight(60);
        $drawing->setCoordinates('A1');
        return $drawing;
    }

    public function collection()
    {
        $export = Keuangan::where('jenis','Pengeluaran')
                    ->whereMonth('tgl',$this->bulan)
                    ->whereYear('tgl',$this->tahun)
                    ->orderBy('tgl','desc')
                    ->get(['tgl','jenis','jumlah','keterangan']);
        return $export;
    }

    public function headings(): array
    {
        return 
        [ 
            [''],[''],[''],[''],
            ['Data Pengeluaran Keuangan'],
            ['Jmlh.Keuangan',$this->countallmoney(),'Jmlh.Infaq',$this->countmoney()],
            ['Tanggal','Jenis','Jumlah','Keterangan']
        ];
    }
}
