<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Jamaah extends Model
{
    protected $table = 'jamaah_aktif';

    protected $fillable =[
        'nama_lengkap','keterangan','status'
    ];
}
