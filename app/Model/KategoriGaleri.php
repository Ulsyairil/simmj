<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Galeri;

class KategoriGaleri extends Model
{
    protected $table = 'kategori_galeri';
    
    protected $fillable = [
        'kategori'
    ];

    public function galeri()
    {
        return $this->belongsTo(Galeri::class);
    }
}
