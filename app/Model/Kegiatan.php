<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\KategoriKegiatan;

class Kegiatan extends Model
{
    protected $table = 'kegiatan';

    protected $fillable = [
        'kategori_kegiatan_id','tgl_mulai', 'tgl_selesai', 'wkt_mulai', 'wkt_selesai', 'judul_kegiatan', 'foto_kegiatan', 'konten_kegiatan'
    ];
    
    public function kategorikegiatan()
    {
        return $this->hasOne(KategoriKegiatan::class,'id','kategori_kegiatan_id');
    }
}
