<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Keuangan extends Model
{
    use Sortable;

    protected $table = 'keuangan';

    protected $fillable = [
        'tgl', 'jenis', 'jumlah', 'keterangan'
    ];

    public $sortable = ['tgl','jenis','jumlah','keterangan'];
}
