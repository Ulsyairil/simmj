<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Kegiatan;

class KategoriKegiatan extends Model
{
    protected $table = 'kategori_kegiatan';
    
    protected $fillable = [
        'kategori'
    ];

    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class);
    }
}
