<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pengurus extends Model
{
    protected $table = 'pengurus';

    protected $fillable = [
        'nama_lengkap', 'foto', 'jabatan'
    ];
}
