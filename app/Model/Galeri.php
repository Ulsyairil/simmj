<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\KategoriGaleri;

class Galeri extends Model
{
    protected $table = 'galeri';

    protected $fillable = [
        'foto','kategori_galeri_id'
    ];

    public function kategorigaleri()
    {
        return $this->hasOne(KategoriGaleri::class, 'id', 'kategori_galeri_id');
    }
}
