<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Carbon;
use App\Model\Kegiatan;
use App\Model\KategoriKegiatan;

class KegiatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('sekretaris');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Kegiatan::orderBy('created_at','desc')->paginate(10);
        $resultkategorikegiatan = KategoriKegiatan::all();
        return view('system/kegiatan/index',compact('result','resultkategorikegiatan'));
    }

    public function filter(Request $request)
    {
        $result = Kegiatan::where('kategori_kegiatan_id', $request->select_kategori)->paginate(10);
        $resultkategorikegiatan = KategoriKegiatan::all();
        $result->withPath('/filter?select_kategori='.$request->select_kategori);
        return view('system/kegiatan/index',compact('result','resultkategorikegiatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'input_kategori' => 'required',
            'input_tanggal_mulai' => 'required',
            'input_tanggal_selesai' => 'required',
            'input_waktu_mulai' => 'required',
            'input_waktu_selesai' => 'required',
            'input_judul' => 'required',
            'textarea_konten' => 'required',
        ]);
        if ($image = $request->foto) {
            $filename = time().'.'.$image->getClientOriginalExtension();
			$destination = 'img/kegiatan/';
            $photopath = $destination . $filename;
            $data = [
                'kategori_kegiatan_id' => $request->input_kategori,
                'tgl_mulai' => $request->input_tanggal_mulai,
                'tgl_selesai' => $request->input_tanggal_selesai,
                'wkt_mulai' => $request->input_waktu_mulai,
                'wkt_selesai' => $request->input_waktu_selesai,
                'judul_kegiatan' => $request->input_judul,
                'foto_kegiatan' => $photopath,
                'konten_kegiatan' => $request->textarea_konten,
                'link_daftar_kegiatan' => $request->input_link_daftar,
                'created_at' => Carbon::now(),
            ];
            // return Response::json($data);
            if (Kegiatan::insert($data)) {
                $image->move($destination, $filename);
                return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
            } else {
                return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
            }
        } else {
            $data = [
                'kategori_kegiatan_id' => $request->input_kategori,
                'tgl_mulai' => $request->input_tanggal_mulai,
                'tgl_selesai' => $request->input_tanggal_selesai,
                'wkt_mulai' => $request->input_waktu_mulai,
                'wkt_selesai' => $request->input_waktu_selesai,
                'judul_kegiatan' => $request->input_judul,
                'konten_kegiatan' => $request->textarea_konten,
                'link_daftar_kegiatan' => $request->input_link_daftar,
                'created_at' => Carbon::now(),
            ];
            // return Response::json($data);
            if (Kegiatan::insert($data)) {
                return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
            } else {
                return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'input_kategori' => 'required',
            'input_tanggal_mulai' => 'required',
            'input_tanggal_selesai' => 'required',
            'input_waktu_mulai' => 'required',
            'input_waktu_selesai' => 'required',
            'input_judul' => 'required',
            'textarea_konten' => 'required',
        ]);
        $table = Kegiatan::where('id',$request->id)->first();
        $path = public_path($table->foto_kegiatan);
        if (File::exists($path)) {
            if ($image = $request->foto) {
                File::delete($path);
                $filename = time().'.'.$image->getClientOriginalExtension();
                $destination = 'img/kegiatan/';
                $photoname = $destination . $filename;
                $data = [
                    'kategori_kegiatan_id' => $request->input_kategori,
                    'tgl_mulai' => $request->input_tanggal_mulai,
                    'tgl_selesai' => $request->input_tanggal_selesai,
                    'wkt_mulai' => $request->input_waktu_mulai,
                    'wkt_selesai' => $request->input_waktu_selesai,
                    'judul_kegiatan' => $request->input_judul,
                    'foto_kegiatan' => $photoname,
                    'konten_kegiatan' => $request->textarea_konten,
                    'link_daftar_kegiatan' => $request->input_link_daftar,
                    'updated_at' => Carbon::now(),
                ];
                if (Kegiatan::where('id',$request->id)->update($data)) {
                    $image->move($destination, $filename);
                    return redirect()->back()->with('alert-success', 'Berhasil Diubah');
                } else {
                    return redirect()->back()->with('alert-danger', 'Gagal Diubah');
                }
            } else {
                $data = [
                    'kategori_kegiatan_id' => $request->input_kategori,
                    'tgl_mulai' => $request->input_tanggal_mulai,
                    'tgl_selesai' => $request->input_tanggal_selesai,
                    'wkt_mulai' => $request->input_waktu_mulai,
                    'wkt_selesai' => $request->input_waktu_selesai,
                    'judul_kegiatan' => $request->input_judul,
                    'foto_kegiatan' => $table->foto_kegiatan,
                    'konten_kegiatan' => $request->textarea_konten,
                    'link_daftar_kegiatan' => $request->input_link_daftar,
                    'updated_at' => Carbon::now(),
                ];
                if (Kegiatan::where('id',$request->id)->update($data)) {
                    return redirect()->back()->with('alert-success', 'Berhasil Diubah');
                } else {
                    return redirect()->back()->with('alert-danger', 'Gagal Diubah');
                }
            }
        } else {
            return redirect()->back()->with('alert-danger', 'Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table = Kegiatan::where('id',$id)->first();
        $image = public_path($table->foto_kegiatan);
        if (File::exists($image)) {
            if (File::delete($image)) {
                Kegiatan::where('id',$id)->delete();
                return redirect()->back()->with('alert-success','Data Berhasil Dihapus');
            } else {
                Kegiatan::where('id',$id)->delete();
                return redirect()->back()->with('alert-success','Data Berhasil Dihapus');
            }
        } else {
            Kegiatan::where('id',$id)->delete();
            return redirect()->back()->with('alert-danger','Data Gagal Dihapus');
        }
    }
}
