<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use App\Model\Jamaah;

class JamaatController extends Controller
{
    public function __construct()
    {
        $this->middleware('sekretaris');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Jamaah::orderBy('created_at','desc')->paginate(10);
        return view('system/jamaah-aktif/index',compact('result'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'input_nama_lengkap' => 'required',
            'textarea_keterangan' => 'required',
            'select_status' => 'required',
        ]);
        $data = [
            'nama_lengkap' => $request->input_nama_lengkap,
            'keterangan' => $request->textarea_keterangan,
            'status' => $request->select_status,
            'created_at' => Carbon::now(),
        ];
        if (Jamaah::insert($data)) {
            return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
        } else {
            return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'input_nama_lengkap' => 'required',
            'textarea_keterangan' => 'required',
            'select_status' => 'required',
        ]);
        $table = Jamaah::where('id',$request->id)->first();
        $data = [
            'nama_lengkap' => $request->input_nama_lengkap,
            'keterangan' => $request->textarea_keterangan,
            'status' => $request->select_status,
            'updated_at' => Carbon::now(),
        ];
        if ($table->update($data)) {
            return redirect()->back()->with('alert-success', 'Data Berhasil Diubah');
        } else {
            return redirect()->back()->with('alert-danger', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Jamaah::where('id',$id)->delete()) {
            return redirect()->back()->with('alert-success', 'Data Berhasil Dihapus');
        } else {
            return redirect()->back()->with('alert-danger', 'Data Gagal Dihapus');
        }
    }
}
