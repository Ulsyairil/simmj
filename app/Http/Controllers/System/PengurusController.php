<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Carbon;
use App\Model\Pengurus;

class PengurusController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Pengurus::orderBy('created_at','asc')->paginate(10);
        return view('system/pengurus/index',compact('result'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'input_jabatan' => 'required',
        ]);
        if ($image = $request->foto) {
            $filename = time().'.'.$image->getClientOriginalExtension();
			$destination = 'img/pengurus/';
            $photopath = $destination . $filename;
            $data = [
                'nama_lengkap' => $request->input_nama,
                'foto' => $photopath,
                'jabatan' => $request->input_jabatan,
                'created_at' => Carbon::now(),
            ];
            if (Pengurus::insert($data)) {
                $image->move($destination, $filename);
                return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
            } else {
                return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
            }
        } else {
            $data = [
                'nama_lengkap' => $request->input_nama,
                'foto' => $image,
                'jabatan' => $request->input_jabatan,
                'created_at' => Carbon::now(),
            ];
            if (Pengurus::insert($data)) {
                return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
            } else {
                return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'input_jabatan' => 'required',
        ]);
        $table = Pengurus::where('id',$request->id)->first();
        $path = public_path($table->foto);
        if (File::exists($path)) {
            if ($image = $request->foto) {
                File::delete($path);
                $filename = time().'.'.$image->getClientOriginalExtension();
                $destination = 'img/pengurus/';
                $photopath = $destination . $filename;
                $data = [
                    'nama_lengkap' => $request->input_nama,
                    'foto' => $photopath,
                    'jabatan' => $request->input_jabatan,
                    'updated_at' => Carbon::now(),
                ];
                if (Pengurus::where('id',$request->id)->update($data)) {
                    $image->move($destination, $filename);
                    return redirect()->back()->with('alert-success', 'Berhasil Diubah');
                } else {
                    return redirect()->back()->with('alert-danger', 'Gagal Diubah');                    
                }
            } else {
                $data = [
                    'nama_lengkap' => $request->input_nama,
                    'foto' => $table->foto,
                    'jabatan' => $request->input_jabatan,
                    'updated_at' => Carbon::now(),
                ];
                if (Pengurus::where('id',$request->id)->update($data)) {
                    return redirect()->back()->with('alert-success', 'Berhasil Diubah');
                } else {
                    return redirect()->back()->with('alert-danger', 'Gagal Diubah');                    
                }
            }
        } else {
            return redirect()->back()->with('alert-danger', 'Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table = Pengurus::where('id',$id)->first();
        $image = public_path($table->foto);
        if (File::exists($image)) {
            if (File::delete($image)) {
                Pengurus::where('id',$id)->delete();
                return redirect()->back()->with('alert-success','Data Berhasil Dihapus');
            } else {
                Pengurus::where('id',$id)->delete();
                return redirect()->back()->with('alert-warning','Data Berhasil Dihapus');
            }
        } else {
            return redirect()->back()->with('alert-danger','Data Gagal Dihapus');
        }
    }
}
