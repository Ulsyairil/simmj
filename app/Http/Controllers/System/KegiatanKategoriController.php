<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\KategoriKegiatan;
use Illuminate\Support\Facades\Redirect;

class KegiatanKategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('sekretaris');
    }

    public function index()
    {
        $result = KategoriKegiatan::paginate(5);
        return view('system/kategori/kategorikegiatan', compact('result'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'input_kategori' => 'required'
        ]);

        $data = [
            'kategori' => $request->input_kategori
        ];

        $insert = KategoriKegiatan::insert($data);
        if ($insert) {
            return Redirect::back()->with('alert-success', 'Berhasil Simpan');
        } else {
            return Redirect::back()->with('alert-success', 'Gagal Simpan');
        }
    }

    public function update(Request $request)
    {
        $request->validate([
            'input_kategori' => 'required'
        ]);

        $data = [
            'kategori' => $request->input_kategori
        ];

        $table = KategoriKegiatan::where('id', $request->id);
        $insert = $table->update($data);
        if ($insert) {
            return Redirect::back()->with('alert-success', 'Berhasil Simpan');
        } else {
            return Redirect::back()->with('alert-success', 'Gagal Simpan');
        }
    }

    public function destroy(Request $request)
    {
        $table = KategoriKegiatan::where('id', $request->id);
        $delete = $table->delete();
        if ($delete) {
            return Redirect::back()->with('alert-success', 'Berhasil Hapus');
        } else {
            return Redirect::back()->with('alert-success', 'Gagal Hapus');
        }
    }
}
