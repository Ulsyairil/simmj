<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PenerimaanKeuanganExport;
use App\Exports\PengeluaranKeuanganExport;
use App\Exports\ZakatKeuanganExport;
use App\Exports\InfaqKeuanganExport;
use Illuminate\Support\Carbon;

class EksporController extends Controller
{
    public function __construct()
    {
        $this->middleware('bendahara');
    }

    public function exportPenerimaan(Request $request)
    {
        $date = Carbon::now();
        $export = Excel::download(new PenerimaanKeuanganExport($request->select_bulan,$request->select_tahun), 'penerimaan-keuangan-'.$date.'.xlsx');
        return $export;
    }

    public function exportPengeluaran(Request $request)
    {
        $date = Carbon::now();
        $exports = Excel::download(new PengeluaranKeuanganExport($request->select_bulan,$request->select_tahun), 'pengeluaran-keuangan-'.$date.'.xlsx');
        return $exports;
    }

    public function exportZakat(Request $request)
    {
        $date = Carbon::now();
        $exports = Excel::download(new ZakatKeuanganExport($request->select_bulan,$request->select_tahun), 'zakat-keuangan-'.$date.'.xlsx');
        return $exports;
    }

    public function exportInfaq(Request $request)
    {
        $date = Carbon::now();
        $exports = Excel::download(new InfaqKeuanganExport($request->select_bulan,$request->select_tahun), 'infaq-keuangan-'.$date.'.xlsx');
        return $exports;
    }
}
