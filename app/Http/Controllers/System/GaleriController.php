<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Carbon;
use App\Model\Galeri;
use App\Model\KategoriGaleri;

class GaleriController extends Controller
{
    public function __construct()
    {
        $this->middleware('sekretaris');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = Galeri::orderBy('created_at','desc')->paginate(10);
        $resultkategorigaleri = KategoriGaleri::all();
        return view('system/galeri/index',compact('result','resultkategorigaleri'));
    }

    public function filter(Request $request)
    {
        $result = Galeri::where('kategori_galeri_id', $request->select_kategori)->paginate(10);
        $resultkategorigaleri = KategoriGaleri::all();
        $result->withPath('/filter?select_kategori='.$request->select_kategori);
        return view('system/galeri/index',compact('result','resultkategorigaleri'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'input_foto' => 'required|image|mimes:jpg,jpeg,png|max:2048',
           'input_kategori' => 'required'
        ]);
        if ($image = $request->input_foto) {
            $filename = time().'.'.$image->getClientOriginalExtension();
			$destination = 'img/galeri/';
            $photopath = $destination . $filename;
            $data = [
                'foto' => $photopath,
                'kategori_galeri_id' => $request->input_kategori,
                'created_at' => Carbon::now()
            ];
            if (Galeri::insert($data)) {
                $image->move($destination, $filename);
                return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
            } else {
                return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
            }
        } else {
            return redirect()->back()->with('alert-danger','Upload Foto Gagal');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $table = Galeri::where('id',$request->id)->first();
        $path = public_path($table->foto);
        if (File::exists($path)) {
            if ($image = $request->input_foto) {
                $request->validate([
                    'input_foto' => 'required|image|mimes:jpg,jpeg,png|max:2048',
                    'input_kategori' => 'required'
                ]);
                File::delete($path);
                $filename = time().'.'.$image->getClientOriginalExtension();
                $destination = 'img/galeri/';
                $photoname = $destination . $filename;
                $data  = [
                    'foto' => $photoname,
                    'kategori_galeri_id' => $request->input_kategori,
                    'updated_at' => Carbon::now()
                ];
                if (Galeri::where('id',$request->id)->update($data)) {
                    $image->move($destination, $filename);
                    return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
                } else {
                    return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
                }
            } else {
                $request->validate([
                    'input_kategori' => 'required'
                ]);
                $data  = [
                    'foto' => $table->foto,
                    'kategori_galeri_id' => $request->input_kategori,
                    'updated_at' => Carbon::now()
                ];
                if (Galeri::where('id',$request->id)->update($data)) {
                    return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
                } else {
                    return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
                }
            } 
        } else {
            return redirect()->back()->with('alert-danger','Gagal Disimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table = Galeri::where('id',$id)->first();
        $image = public_path($table->foto);
        if (File::exists($image)) {
            if (File::delete($image)) {
                Galeri::where('id',$id)->delete();
                return redirect()->back()->with('alert-success','Data Berhasil Dihapus');
            } else {
                Galeri::where('id',$id)->delete();
                return redirect()->back()->with('alert-warning','Data Berhasil Dihapus');
            }
        } else {
            Galeri::where('id',$id)->delete();
            return redirect()->back()->with('alert-danger','Data Gagal Dihapus');
        }
    }
}
