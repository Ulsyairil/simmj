<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Carbon;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PenggunaController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = User::where('id', $id)
                    ->get();
        return view('system/pengguna/edit',compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'input_nama_lengkap' => 'required',
            'input_username' => 'required',
            'input_password' => 'required',
        ]);
        $encrypt = Crypt::encrypt($request->input_password);
        $table = User::where('id',$request->id);
        $data = [
            'name' => $request->input_nama_lengkap,
            'username' => $request->input_username,
            'password' => $encrypt,
            'updated_at' => Carbon::now(),
        ];
        if ($table->update($data)) {
            Auth::logout();
            Session::flush();
            return redirect()->route('login')->with('alert-success', 'Silahkan Login Kembali');
        } else {
            return redirect()->back()->with('alert-danger', 'Data Gagal Diubah');
        }
    }
}
