<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Model\Jamaah;
use App\Model\Kegiatan;
use App\Model\Keuangan;
use App\Model\Galeri;

class HomeController extends Controller
{
    private function countmoney() 
    {
        $penerimaan = Keuangan::where('jenis','=','Penerimaan')
                ->sum('jumlah');
        $pengeluaran = Keuangan::where('jenis','=','Pengeluaran')
                ->sum('jumlah');
        $zakat = Keuangan::where('jenis','=','Zakat')
                ->sum('jumlah');
        $infaq = Keuangan::where('jenis','=','Infaq')
                ->sum('jumlah');
        $hitung = $penerimaan + $zakat + $infaq - $pengeluaran;
        $format_hitung = number_format($hitung,0,",",".");
        return $format_hitung;
    }

    public function chartPenerimaan()
    {
        $result = Keuangan::where('jenis','=','Penerimaan')
                    ->orderBy('tgl', 'ASC')
                    ->get();
        return response()->json($result);
    }

    public function chartPengeluaran()
    {
        $result = Keuangan::where('jenis','=','Pengeluaran')
                    ->orderBy('tgl', 'ASC')
                    ->get();
        return response()->json($result);
    }

    public function chartZakat()
    {
        $result = Keuangan::where('jenis','=','Zakat')
                    ->orderBy('tgl', 'ASC')
                    ->get();
        return response()->json($result);
    }

    public function chartInfaq()
    {
        $result = Keuangan::where('jenis','=','Infaq')
                    ->orderBy('tgl', 'ASC')
                    ->get();
        return response()->json($result);
    }

    public function index()
    {
        $jamaahaktif = Jamaah::count();
        $hasilkeuangan = $this->countmoney();
        $kegiatan = Kegiatan::count();
        $galeri = Galeri::count();
        return view('system/index', compact('jamaahaktif','hasilkeuangan','kegiatan','galeri'));
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('login')->with('alert-success', 'Anda Telah Logout');
    }
}
