<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use App\Model\Keuangan;

class KeuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('bendahara');
    }

    private function countmoney() 
    {
        $penerimaan = Keuangan::where('jenis','=','Penerimaan')
                ->sum('jumlah');
        $pengeluaran = Keuangan::where('jenis','=','Pengeluaran')
                ->sum('jumlah');
        $zakat = Keuangan::where('jenis','=','Zakat')
                ->sum('jumlah');
        $infaq = Keuangan::where('jenis','=','Infaq')
                ->sum('jumlah');
        $hitung = $penerimaan + $zakat + $infaq - $pengeluaran;
        $format_hitung = number_format($hitung,0,",",".");
        return $format_hitung;
    }

    private function countPenerimaan()
    {
        $count = Keuangan::where('jenis','=','Penerimaan')
                ->sum('jumlah');
        $count_format = number_format($count,0,",",".");
        return $count_format;
    }

    private function countPengeluaran()
    {
        $count = Keuangan::where('jenis','=','Pengeluaran')
                ->sum('jumlah');
        $count_format = number_format($count,0,",",".");
        return $count_format;
    }

    private function countZakat()
    {
        $count = Keuangan::where('jenis','=','Zakat')
                ->sum('jumlah');
        $count_format = number_format($count,0,",",".");
        return $count_format;
    }

    private function countInfaq()
    {
        $count = Keuangan::where('jenis','=','Infaq')
                ->sum('jumlah');
        $count_format = number_format($count,0,",",".");
        return $count_format;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPenerimaan()
    {
        $resultmoney = $this->countmoney();
        $resultpenerimaan = $this->countPenerimaan();
        $result = Keuangan::sortable()
                    ->orderBy('tgl','desc')
                    ->where('jenis','=','Penerimaan')
                    ->paginate(10);
        return view('system/keuangan/index-penerimaan',compact('result','resultmoney','resultpenerimaan'));
    }

    public function indexPengeluaran()
    {
        $resultmoney = $this->countmoney();
        $resultpengeluaran = $this->countPengeluaran();
        $result = Keuangan::sortable()
                    ->orderBy('tgl','desc')
                    ->where('jenis','=','Pengeluaran')
                    ->paginate(10);
        return view('system/keuangan/index-pengeluaran',compact('result','resultmoney','resultpengeluaran'));
    }

    public function indexZakat()
    {
        $resultmoney = $this->countmoney();
        $resultzakat = $this->countZakat();
        $result = Keuangan::sortable()
                    ->orderBy('tgl','desc')
                    ->where('jenis','=','Zakat')
                    ->paginate(10);
        return view('system/keuangan/index-zakat',compact('result','resultmoney','resultzakat'));
    }

    public function indexInfaq()
    {
        $resultmoney = $this->countmoney();
        $resultinfaq = $this->countInfaq();
        $result = Keuangan::sortable()
                    ->orderBy('tgl','desc')
                    ->where('jenis','=','Infaq')
                    ->paginate(10);
        return view('system/keuangan/index-infaq',compact('result','resultmoney','resultinfaq'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'input_tanggal' => 'required',
            'input_jenis' => 'required',
            'input_jumlah' => 'required',
            'textarea_keterangan' => 'required'
        ]);
        $data = [
            'tgl' => $request->input_tanggal,
            'jenis' => $request->input_jenis,
            'jumlah' => $request->input_jumlah,
            'keterangan' => $request->textarea_keterangan,
            'created_at' => Carbon::now(),
        ];
        if (Keuangan::insert($data)) {
            return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
        } else {
            return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
        }
    }

    public function update(Request $request)
    {
        $request->validate([
            'input_tanggal' => 'required',
            'input_jenis' => 'required',
            'input_jumlah' => 'required',
            'textarea_keterangan' => 'required'
        ]);
        $table = Keuangan::where('id',$request->id);
        $data = [
            'tgl' => $request->input_tanggal,
            'jenis' => $request->input_jenis,
            'jumlah' => $request->input_jumlah,
            'keterangan' => $request->textarea_keterangan,
            'created_at' => Carbon::now(),
        ];
        if ($table->update($data)) {
            return redirect()->back()->with('alert-success', 'Berhasil Disimpan');
        } else {
            return redirect()->back()->with('alert-danger', 'Gagal Disimpan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Keuangan::where('id',$id)->delete()) {
            return redirect()->back()->with('alert-success', 'Berhasil Dihapus');
        } else {
            return redirect()->back()->with('alert-danger', 'Data Gagal Dihapus');
        }
    }
}
