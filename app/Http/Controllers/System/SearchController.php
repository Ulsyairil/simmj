<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Jamaah;
use App\Model\Keuangan;
use App\Model\Kegiatan;
use App\Model\Pengurus;
use App\Model\KategoriKegiatan;

class SearchController extends Controller
{
    private function countmoney() 
    {
        $penerimaan = Keuangan::where('jenis','=','Penerimaan')
                ->sum('jumlah');
        $pengeluaran = Keuangan::where('jenis','=','Pengeluaran')
                ->sum('jumlah');
        $zakat = Keuangan::where('jenis','=','Zakat')
                ->sum('jumlah');
        $infaq = Keuangan::where('jenis','=','Infaq')
                ->sum('jumlah');
        $hitung = $penerimaan + $zakat + $infaq - $pengeluaran;
        $format_hitung = number_format($hitung,0,",",".");
        return $format_hitung;
    }

    private function countPenerimaan()
    {
        $count = Keuangan::where('jenis','=','Penerimaan')
                ->sum('jumlah');
        $count_format = number_format($count,0,",",".");
        return $count_format;
    }

    private function countPengeluaran()
    {
        $count = Keuangan::where('jenis','=','Pengeluaran')
                ->sum('jumlah');
        $count_format = number_format($count,0,",",".");
        return $count_format;
    }

    private function countZakat()
    {
        $count = Keuangan::where('jenis','=','Zakat')
                ->sum('jumlah');
        $count_format = number_format($count,0,",",".");
        return $count_format;
    }

    private function countInfaq()
    {
        $count = Keuangan::where('jenis','=','Infaq')
                ->sum('jumlah');
        $count_format = number_format($count,0,",",".");
        return $count_format;
    }

    public function cariJamaah(Request $request)
    {
        $cari = $request->input_cari;
        $result = Jamaah::where('nama_lengkap','LIKE','%'.$cari.'%')
                            ->orderBy('created_at','desc')
                            ->paginate('10');
        return view('system/jamaah-aktif/index',compact('result'));
    }

    public function cariKegiatan(Request $request)
    {
        $cari = $request->input_cari;
        $result = Kegiatan::where('judul_kegiatan','LIKE','%'.$cari.'%')
                            ->orderBy('created_at','desc')
                            ->paginate('10');
        $resultkategorikegiatan = KategoriKegiatan::all();
        $result->withPath('/cari?input_cari='.$request->input_cari);
        return view('system/kegiatan/index',compact('result','resultkategorikegiatan'));
    }

    public function cariKeuanganPenerimaan(Request $request)
    {
        $resultmoney = $this->countmoney();
        $resultpenerimaan = $this->countPenerimaan();
        $cari = $request->input_cari;
        $result = Keuangan::where('keterangan','LIKE','%'.$cari.'%')
                            ->where('jenis','=','Penerimaan')
                            ->orderBy('tgl','desc')
                            ->paginate('10');
        return view('system/keuangan/index-penerimaan',compact('result','resultmoney','resultpenerimaan'));
    }

    public function cariKeuanganPengeluaran(Request $request)
    {
        $resultmoney = $this->countmoney();
        $resultpengeluaran = $this->countPengeluaran();
        $cari = $request->input_cari;
        $result = Keuangan::where('keterangan','LIKE','%'.$cari.'%')
                            ->where('jenis','=','Pengeluaran')
                            ->orderBy('tgl','desc')
                            ->paginate('10');
        return view('system/keuangan/index-pengeluaran',compact('result','resultmoney','resultpengeluaran'));
    }

    public function cariKeuanganZakat(Request $request)
    {
        $resultmoney = $this->countmoney();
        $resultzakat = $this->countZakat();
        $cari = $request->input_cari;
        $result = Keuangan::where('keterangan','LIKE','%'.$cari.'%')
                            ->where('jenis','=','Zakat')
                            ->orderBy('tgl','desc')
                            ->paginate('10');
        return view('system/keuangan/index-zakat',compact('result','resultmoney','resultzakat'));
    }

    public function cariKeuanganInfaq(Request $request)
    {
        $resultmoney = $this->countmoney();
        $resultinfaq = $this->countInfaq();
        $cari = $request->input_cari;
        $result = Keuangan::where('keterangan','LIKE','%'.$cari.'%')
                            ->where('jenis','=','Infaq')
                            ->orderBy('tgl','desc')
                            ->paginate('10');
        return view('system/keuangan/index-infaq',compact('result','resultmoney','resultinfaq'));
    }

    public function cariPengurus(Request $request)
    {
        $cari = $request->input_cari;
        $result = Pengurus::where('jabatan','LIKE','%'.$cari.'%')
                            ->orderBy('created_at','asc')
                            ->paginate('10');
        return view('system/pengurus/index',compact('result'));
    }
}
