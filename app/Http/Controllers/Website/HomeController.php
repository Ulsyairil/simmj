<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Kegiatan;
use App\Model\Galeri;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class HomeController extends Controller
{
    public function index()
    {
        $resultEvent = $this->showEvent();
        $resultGallery = $this->showGallery();
        return view('website/index', compact('resultEvent','resultGallery'));
    }

    private function showEvent()
    {
        $showEvent = Kegiatan::orderBy('created_at','desc')->paginate();
        return $showEvent;
    }

    private function showGallery()
    {
        $showGallery = Galeri::orderBy('created_at','desc')->paginate();
        return $showGallery;
    }

    public function mailFeedback(Request $request)
    {
        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'mail.masjidjabalussuada.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'no-reply@masjidjabalussuada.com';
            $mail->Password = '6EyahwkcjCxT';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            $mail->setFrom('no-reply@masjidjabalussuada.com', "No-reply Message Send");
            $mail->addAddress('info@masjidjabalussuada.com', "Admin Masjid Jabalussu'ada");

            $mail->isHTML(true); 																	// Set email format to HTML
            $mail->Subject = 'Feedback : '.$request->input_subject.' ('.$request->input_name.')';
            $mail->Body    = 'Name : '.$request->input_name.'<br><br>'.'Email : '.$request->input_email.'<br><br>'.$request->input_message;
            if ($mail->send()) {
                return redirect()->back()->with('alert-success','Thanks for your feedback');
            } else {
                return redirect()->back()->with('alert-danger',"Sorry, your feedback can't send");
            }
        } catch (Exception $e) {
            return redirect()->back()->with('alert-danger','Message could not be sent.');
        }
    }
}
