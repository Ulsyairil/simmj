<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Kegiatan;
use App\Model\KategoriKegiatan;

class EventController extends Controller
{
    public function index()
    {
        $result = Kegiatan::orderBy('created_at','desc')->paginate(16);
        $resultkategorikegiatan = KategoriKegiatan::all();
        return view('website/events/index',compact('result','resultkategorikegiatan'));
    }

    public function searchindex(Request $request)
    {
        $cari = $request->input_cari;
        $result = Kegiatan::where('judul_kegiatan','LIKE','%'.$cari.'%')
                            ->orderBy('created_at','desc')
                            ->paginate(16);
        $resultkategorikegiatan = KategoriKegiatan::all();
        $result->withPath('/filter?select_kategori='.$request->select_kategori);
        return view('website/events/index',compact('result','resultkategorikegiatan'));
    }

    public function filter(Request $request)
    {
        $result = Kegiatan::where('kategori_kegiatan_id', $request->select_kategori)
                            ->orderBy('created_at','desc')
                            ->paginate(16);
        $resultkategorikegiatan = KategoriKegiatan::all();
        $result->withPath('/filter?select_kategori='.$request->select_kategori);
        return view('website/events/index',compact('result','resultkategorikegiatan'));
    }
}
