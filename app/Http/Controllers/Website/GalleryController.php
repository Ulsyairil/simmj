<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Galeri;
use App\Model\KategoriGaleri;

class GalleryController extends Controller
{
    public function index()
    {
        $result = Galeri::orderBy('created_at','desc')->paginate(16);
        $resultkategorigaleri = KategoriGaleri::all();
        return view('website/gallery/index',compact('result','resultkategorigaleri'));
    }

    public function filter(Request $request)
    {
        $result = Galeri::where('kategori_galeri_id', $request->select_kategori)
                            ->orderBy('created_at','desc')
                            ->paginate(16);
        $resultkategorigaleri = KategoriGaleri::all();
        $result->withPath('/filter?select_kategori='.$request->select_kategori);
        return view('website/gallery/index',compact('result','resultkategorigaleri'));
    }
}
