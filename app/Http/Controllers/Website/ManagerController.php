<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Pengurus;

class ManagerController extends Controller
{
    public function index()
    {
        $result = Pengurus::orderBy('created_at','asc')->get();
        return view('website/about-us/index',compact('result'));
    }
}
